echo "[CFFI] {DEBUG} build lib (target = current host)"
cargo build -p cffi
echo "[CFFI-Tests] {DEBUG} build lib (target = current host)"
cargo build -p cffi-tests

echo "make symlinks -> /out/bin/debug/"
ln -sf $(pwd)/$line/target/debug/libgogcffi.a    $(pwd)/$line/out/bin/current/debug/gog.a
ln -sf $(pwd)/$line/target/debug/libcffi_tests.a    $(pwd)/$line/out/bin/current/debug/gog_cffi_tests.a
