//! https://gogapidocs.readthedocs.io/en/latest/galaxy.html#api-gog-com

use std::borrow::Cow;

use app::*;
use error::*;
use net::Request;


pub type Result<T> = ::std::result::Result<T, ApiError>;

impl<T> Into<Result<T>> for ApiError {
	fn into(self) -> Result<T> { Err(self) }
}


/// Returns information about a product.
pub mod products {
	use super::*;
	use ::std::fmt;
	use ::net::PROTOCOL;
	use ds::products::ProductId;
	use traits::IClient;

	/// default host
	const HOST: &str = "api.gog.com";

	#[derive(Debug)]
	pub struct ProductQuery {
		pub locale: Option<()>,
		pub parts: Option<ProductResponseParts>,
	}

	impl Default for ProductQuery {
		fn default() -> Self {
			Self { locale: None,
			       parts: Some(Default::default()), }
		}
	}

	impl fmt::Display for ProductQuery {
		fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
			let mut result = String::new();
			let mut delimiter = "";
			if let Some(parts) = &self.parts {
				result.push_str(&format!("{}{}", delimiter, parts));
				delimiter = "&";
			};

			// TODO: impl for `locale`
			// "?locale="
			// if let Some(locale) = &self.locale {
			// 	result.push_str(&format!("{}{}", delimiter, locale));
			// };

			write!(f, "{}", result)
		}
	}

	#[derive(Debug)]
	pub struct ProductResponseParts {
		pub downloads: bool,
		pub expanded_dlcs: bool,
		pub description: bool,
		pub screenshots: bool,
		pub videos: bool,
		pub related_products: bool,
		pub changelog: bool,
	}

	impl Default for ProductResponseParts {
		fn default() -> Self {
			Self { downloads: true,
			       expanded_dlcs: true,
			       description: true,
			       screenshots: true,
			       videos: true,
			       related_products: true,
			       changelog: true, }
		}
	}

	impl fmt::Display for ProductResponseParts {
		fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
			let mut list: Vec<&str> = Vec::new();
			if self.downloads {
				list.push("downloads")
			}
			if self.expanded_dlcs {
				list.push("expanded_dlcs")
			}
			if self.description {
				list.push("description")
			}
			if self.screenshots {
				list.push("screenshots")
			}
			if self.videos {
				list.push("videos")
			}
			if self.related_products {
				list.push("related_products")
			}
			if self.changelog {
				list.push("changelog")
			}
			if list.len() > 0 {
				write!(f, "expand={}", list.join(","))
			} else {
				write!(f, "")
			}
		}
	}

	pub struct Product {
		pub id: ProductId,
		pub extra: Option<ProductQuery>,
	}
	impl Request for Product {
		type Body = ();
		type Response = ProductInfo;
		fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
			let query = match &self.extra {
				Some(query) => format!("?{}", query),
				None => Default::default(),
			};
			let host = if let Some(config) = client.config() {
				&config.end_points.products_details
			} else {
				HOST
			};
			format!("{}://{}/products/{}{}", PROTOCOL, host, self.id, query).into()
		}
	}

	pub struct Products<T: AsRef<[ProductId]>> {
		pub ids: T,
		pub extra: Option<ProductQuery>,
	}

	impl<T: AsRef<[ProductId]>> Request for Products<T> {
		type Body = ();
		type Response = Vec<ProductInfo>;
		fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
			let query = match &self.extra {
				Some(query) => format!("&{}", query),
				None => Default::default(),
			};
			let host = if let Some(config) = client.config() {
				&config.end_points.products_details
			} else {
				HOST
			};
			let ids = self.ids.as_ref();
			assert!(ids.len() > 0);
			assert!(ids.len() <= 50);
			let ids_str: String = ids.iter().map(|c| format!("{}%2C", *c)).collect();
			format!(
			        "{}://{}/products?ids={}{}",
			        PROTOCOL,
			        host,
			        &ids_str[..(ids_str.len() - 3)],
			        query
			).into()
		}
	}

	#[derive(Serialize, Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct ProductInfo {
		pub id: ProductId,
		// TODO: describe fields
	}
}


/// Requests grouped all about account & user-data.
pub mod account {
	use super::*;


	pub mod library {
		// TODO: MOVE to account::library::
		use super::*;
		use traits::IClient;


		// XXX: TEMP
		use self::query::*;
		pub struct FilteredProducts<T: AsRef<[System]>>(pub Query<T>);
		impl<'de, T: AsRef<[System]>> Request for FilteredProducts<T> {
			type Body = ();
			// type Response = ::ds::userdata::GameIdList;
			type Response = ::serde_json::Value;
			// fn uri<'a>(&self, client: &Client) -> Cow<'a, str> {
			fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
				use ::net::PROTOCOL;
				use ::account::HOST;
				use ::account::library::USED_DATA_GAMES;
				format!("{proto}://{host}/{uri}", proto = PROTOCOL, host = HOST, uri = USED_DATA_GAMES).into()
				// https://embed.gog.com/account/getFilteredProducts?hiddenFlag=0&mediaType=1&page=3&sortBy=date_purchased&totalPages=3
			}
		}
	}
}


pub mod query {


	pub struct Query<List: AsRef<[System]>> {
		sort_by: Option<String>,
		media_type: MediaType,
		system: Option<List>,
		price: Option<Price>,
	}

	impl<T: AsRef<[System]>> Default for Query<T> {
		fn default() -> Self {
			Self { system: None,
			       price: None,
			       media_type: MediaType::Game,
			       sort_by: Some(String::from("date_purchased")), }
		}
	}

	pub enum Filter {
		Hidden(bool),
		MediaType(MediaType),
		System(Vec<u32>),
	}

	#[repr(u32)]
	pub enum MediaType {
		Game = 1,
		Movie = 2,
	}

	#[repr(u32)]
	pub enum System {
		WinXP = 1,
		WinVista = 2,
		Win7 = 4,
		Win8 = 8,
		Win10 = 4096,
		/// OSX 10.6+
		MacOsX6 = 16,
		/// OSX 10.7+
		MacOsX7 = 32,
		/// Ubuntu 14.04
		Ubuntu14 = 1024,
		/// Ubuntu 16.04
		Ubuntu16 = 2048,
		/// Ubuntu 18.04
		Ubuntu18 = 8192,
	}

	#[repr(u32)]
	pub enum Price {
		/// < 1 Eur
		Cheap = 1,
		/// < 700 Rur
		Price2 = 2,
		/// < 1000 Rur
		Price3 = 3,
		/// < 1700 Rur
		Price4 = 4,
		/// > 1700 Rur
		Price5 = 5,
		/// in Sale
		Discounted = 6,
	}

	// impl<T> Query<T> {
	// 	pub fn uri<'a>(&self) -> Cow<'a, str> {
	// 		// getFilteredProducts?hiddenFlag=0&mediaType=1&page=3&sortBy=date_purchased&totalPages=3
	// 		format!("getFilteredProducts?hiddenFlag={hidden_flag}&mediaType={media_type}&page={page}&sortBy={sort_by}&totalPages={total_pages}",
	// 		        hidden_flag = false,
	// 		        media_type = 1,
	// 		      //   sort_by = &self.sort_by.unwrap(), // _or("null".to_string()
	// 		        sort_by = "date_purchased", // _or("null".to_string()
	// 		        page = 1,
	// 		        total_pages = 0,
	// 				  );
	// 		"".into()
	// 	}
	// }
}
