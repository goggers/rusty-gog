use serde::de::{self, Visitor};
use std::fmt;
use std::u8;

pub struct U8Visitor;
impl<'de> Visitor<'de> for U8Visitor {
	type Value = u8;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result { formatter.write_str("an uinteger between 0 and 2^8") }

	fn visit_u8<E>(self, value: u8) -> Result<Self::Value, E>
		where E: de::Error {
		Ok(value)
	}

	fn visit_u32<E>(self, value: u32) -> Result<Self::Value, E>
		where E: de::Error {
		if value >= u8::MIN as u32 && value <= u8::MAX as u32 {
			Ok(value as u8)
		} else {
			Err(E::custom(format!("u8 out of range: {}", value)))
		}
	}

	fn visit_u64<E>(self, value: u64) -> Result<Self::Value, E>
		where E: de::Error {
		if value >= u8::MIN as u64 && value <= u8::MAX as u64 {
			Ok(value as u8)
		} else {
			Err(E::custom(format!("u8 out of range: {}", value)))
		}
	}
}
