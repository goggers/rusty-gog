// use std::collections::BTreeMap;


pub type GameId = ::ds::products::ProductId;


#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct GameDetails {
	pub title: String,
	pub text_information: String,
	pub changelog: String,

	pub background_image: String,
	pub forum_link: String,

	pub cd_key: String,

	// bools
	pub is_pre_order: bool,
	pub is_base_product_missing: bool,
	pub missing_base_product: Option<::serde_json::Value>, // TODO: type

	pub release_timestamp: i64,
	pub extras: Vec<GameDetailsExtra>,
	pub tags: Vec<::ds::tags::UserTag>,
	pub dlcs: Vec<::serde_json::Value>,     // TODO: type
	pub messages: Vec<::serde_json::Value>, // TODO: type
	pub features: Vec<::serde_json::Value>, // TODO: type
	/// [(Lang, Details)]
	pub downloads: Vec<(String, GameDetailsDownloads)>, // TODO: type
	pub galaxy_downloads: Vec<::serde_json::Value>, // TODO: type
	pub simple_galaxy_installers: Vec<SimpleGalaxyInstaller>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct GameDetailsExtra {
	manual_url: String,
	downloader_url: String,
	name: String, // TODO: enum
	#[serde(rename = "type")]
	kind: String, // TODO: enum
	info: i32,
	size: String, // TODO: custom parse
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct GameDetailsDownloads {
	windows: Option<Vec<GameDownloadsDetails>>,
	linux: Option<Vec<GameDownloadsDetails>>,
	mac: Option<Vec<GameDownloadsDetails>>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct GameDownloadsDetails {
	manual_url: String,
	downloader_url: String,
	name: String, // TODO: enum
	version: String,
	date: String,
	size: String, // TODO: custom parse
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SimpleGalaxyInstaller {
	path: String,
	os: String,
}
