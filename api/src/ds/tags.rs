pub type UserTagId = String; // u64?

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct UserTag {
	id: UserTagId,
	name: String,
	product_count: u64,
	// product_count: String,
}
