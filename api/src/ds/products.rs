pub type ProductId = u64;

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct FilteredProductsResponse {
	pub sort_by: String, // TODO: enum e.g.: "date_purchased"
	pub page: u32,
	pub total_pages: u32,
	pub total_products: u32,
	pub products_per_page: u32,
	pub content_system_compatibility: Option<::serde_json::Value>,
	pub movies_count: u32,
	pub updated_products_count: u32,
	pub hidden_updated_products_count: u32,
	pub tags: Vec<::ds::tags::UserTag>,
	pub products: Vec<::ds::tags::UserTag>,

	pub applied_filters: ::serde_json::Value,
	pub has_hidden_products: bool,
}


#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Product {
	pub is_galaxy_compatible: bool,
	pub tags: Vec<::ds::tags::UserTagId>,
	pub id: ProductId,

	pub availability: ProductAvailability,

	// std
	pub title: String,
	pub image: String,
	pub url: String,
	pub works_on: ProductWorksOn,
	pub category: String, // TODO: enum
	pub rating: u32,      // TODO: enum ?
	pub is_coming_soon: bool,
	pub is_movie: bool,
	pub is_game: bool,
	pub is_new: bool,

	pub slug: String,
	pub updates: u32,
	pub dlc_count: u32,

	pub release_date: ProductReleaseDate,

	pub is_base_product_missing: bool,
	pub is_hiding_disabled: bool,
	pub is_in_development: bool,
	pub is_hidden: bool,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ProductAvailability {
	is_available: bool,
	is_available_in_account: bool,
}


#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "PascalCase")]
pub struct ProductWorksOn {
	windows: bool,
	linux: bool,
	mac: bool,
}


#[derive(Serialize, Deserialize, Debug)]
pub struct ProductReleaseDate {
	date: String,
	timezone_type: u32,
	timezone: String,
}
