use std::borrow::Cow;
use net::Request;
use net::PROTOCOL;
use traits::IClient;


#[derive(Debug)]
pub struct GetConfig();
impl Request for GetConfig {
	type Body = ();
	type Response = ds::Config;
	const AUTH: bool = false;
	/// e.g.: "cfg.gog.com/desktop-galaxy-client/config.json"
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!("{}://{}/{}/{}", PROTOCOL, ds::HOST, client.application(), ds::CONFIG).into()
	}
}


pub mod ds {
	use chrono::UTC;

	pub const HOST: &str = "cfg.gog.com";
	pub(super) const CONFIG: &str = "config.json";


	#[cfg_attr(feature = "cffi", repr(C))]
	#[derive(Serialize, Deserialize, Debug, Clone)]
	#[serde(rename_all = "camelCase")]
	pub struct Config {
		pub status: String,         // TODO: enum with values like "online"
		pub channel: String,        // TODO: enum with values like "production",
		pub talk_interval: u32,     // 600
		pub complain_interval: u32, // 600
		#[serde(rename = "end_points")]
		pub end_points: ConfigEndPoints,
		pub intervals: ConfigIntervals,
		pub timestamp: i32, // always zero incomes, why?

		#[serde(default = "timestamp")]
		pub timestamp_internal: i64,
	}

	impl Config {
		// TODO: needs tests
		pub fn expired(&self) -> bool {
			let now = UTC::now().timestamp();
			now > self.timestamp_internal + self.intervals.self_update_check as i64
		}
	}

	#[cfg_attr(feature = "cffi", repr(C))]
	#[derive(Serialize, Deserialize, Debug, Clone)]
	#[serde(rename_all = "camelCase")]
	pub struct ConfigEndPoints {
		pub files: String,
		pub products: String,
		pub users: String,
		pub auth: String,
		pub cdn: String,
		pub products_details: String,
		pub gameplay: String,
		#[serde(rename = "gog-api")]
		pub gog_api: String,
	}

	#[cfg_attr(feature = "cffi", repr(C))]
	#[derive(Serialize, Deserialize, Debug, Clone)]
	#[serde(rename_all = "camelCase")]
	/// cbindgen:field-names=[quick, presence, short_, normal, self_update_check, long_, eternity]
	pub struct ConfigIntervals {
		pub quick: i32,
		pub presence: i32,
		pub short: i32,
		pub normal: i32,
		pub self_update_check: i32,
		pub long: i32,
		pub eternity: i32,
	}

	fn timestamp() -> i64 { UTC::now().timestamp() }
}
