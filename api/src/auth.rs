use std::borrow::Cow;
use net::{self, PROTOCOL, Request};
use cfg::ds::Config;
use app::Descriptor;
use app::Client;
use traits::IClient;
use self::ds::AuthToken;
use self::ds::HOST;


pub fn get_auth_uri(client: &Client) -> String {
	let host = if let Some(config) = client.config() {
		&config.end_points.auth
	} else {
		HOST
	};
	let descriptor = client.descriptor();
	format!(
	        "{}://{}/auth?client_id={client_id}&redirect_uri={redirect_uri}&response_type=code&layout=client2",
	        PROTOCOL,
	        host, // or "auth.gog.com -> login.gog.com",
	        client_id = descriptor.id,
	        redirect_uri = descriptor.uri
	)
}

pub fn get_auth_uri_d(config: &Option<&Config>, descriptor: &Descriptor) -> String {
	let host = if let Some(config) = config {
		&config.end_points.auth
	} else {
		HOST
	};
	format!(
	        "{}://{}/auth?client_id={client_id}&redirect_uri={redirect_uri}&response_type=code&layout=client2",
	        PROTOCOL,
	        host, // or "auth.gog.com -> login.gog.com",
	        client_id = descriptor.id,
	        redirect_uri = descriptor.uri
	)
}

pub fn is_auth_uri_final<S: AsRef<str>>(uri: S) -> Result<bool, ::url::ParseError> {
	static SEG: &str = "on_login_success";
	Ok(uri.as_ref().contains(SEG) && {
		use ::url::Url;
		let uri: Url = uri.as_ref().parse()?;
		if let Some(segments) = uri.path_segments() {
			for seg in segments {
				if seg == SEG {
					return Ok(true);
				}
			}
		};
		false
	})
}

pub fn parse_auth_code_in_url<S: AsRef<str>>(uri: S) -> Result<String, ::url::ParseError> {
	use ::url::Url;
	static KEY: &str = "code";
	let uri: Url = uri.as_ref().parse()?;
	let pairs = uri.query_pairs();
	for (key, value) in pairs {
		if key == KEY {
			return Ok(value.to_owned().to_string());
		}
	}
	Err(::url::ParseError::InvalidIpv4Address)
}


#[cfg(not(feature = "http"))]
pub fn get_auth_header(token: &AuthToken) -> (&'static str, &str) {
	static NAME: &str = "Authorization";
	return (NAME, token.access_token.value());
}


/// Inner: `code :String` - result of User's login on url from `get_auth_uri`.
pub struct RequestTokenByCode(pub String);
impl Request for RequestTokenByCode {
	const AUTH: bool = false;
	type Body = ();
	type Response = AuthToken;
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		let host = if let Some(config) = client.config() {
			&config.end_points.auth
		} else {
			HOST
		};
		let descriptor = client.descriptor();
		format!("{}://{}/token?client_id={client_id}&client_secret={client_secret}&grant_type=authorization_code&code={code}&redirect_uri={redirect_uri}",
		        PROTOCOL,
		        host,
		        client_id = descriptor.id,
		        client_secret = descriptor.secret,
		        code = self.0.trim(),
		        redirect_uri = descriptor.uri ).into()
	}
}


/// Refresh stale token.
pub struct RefreshToken();
impl Request for RefreshToken {
	type Body = ();
	type Response = AuthToken;
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		let host = if let Some(config) = client.config() {
			&config.end_points.auth
		} else {
			HOST
		};
		let descriptor = client.descriptor();
		format!("{}://{}/token?client_id={client_id}&client_secret={client_secret}&grant_type=refresh_token&refresh_token={refresh_token}",
		        PROTOCOL,
		        host,
		        client_id = descriptor.id,
		        client_secret = descriptor.secret,
		        refresh_token = if let Some(token) = &client.auth_token() {
			        &token.refresh_token
			     } else {
			        panic!("U havnt token! Ha-ha!");
			     } ).into()
	}
}


pub mod ds {
	use chrono::UTC;
	use app::TokenState;
	use user::GalaxyUserID;
	use super::net::headers::Bearer;
	use serde::{Serializer, Deserialize, Deserializer};

	/// default host
	pub(super) const HOST: &str = "auth.gog.com";


	#[cfg_attr(feature = "cffi", repr(C))]
	#[derive(Serialize, Deserialize, Debug, Clone)]
	/// Main auth token
	pub struct AuthToken {
		#[serde(serialize_with = "se_bearer")]
		#[serde(deserialize_with = "de_bearer")]
		pub access_token: Bearer,
		pub expires_in: u16,
		pub token_type: String,
		pub scope: String,
		pub session_id: String,
		pub refresh_token: String,
		pub user_id: GalaxyUserID,

		#[serde(default = "timestamp")]
		pub timestamp: i64,
	}

	impl AuthToken {
		pub fn expired(&self) -> bool {
			let now = UTC::now().timestamp();
			now > self.timestamp + self.expires_in as i64
		}

		pub fn state(t: &Option<&Self>) -> TokenState {
			use app::TokenState::*;
			match t {
				Some(token) => {
					if token.expired() {
						Expired
					} else {
						Normal
					}
				},
				None => Missed,
			}
		}
	}

	fn timestamp() -> i64 { UTC::now().timestamp() }

	pub fn de_bearer<'de, D>(deserializer: D) -> Result<Bearer, D::Error>
		where D: Deserializer<'de> {
		Ok(Bearer::new(&String::deserialize(deserializer)?))
	}

	pub fn se_bearer<S>(v: &Bearer, serializer: S) -> Result<S::Ok, S::Error>
		where S: Serializer {
		serializer.serialize_str(v.token())
	}
}
