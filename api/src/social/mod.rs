// https://menu.gog.com/v1/account/basic?locale=en-US&currency=RUB&country=RU
/* e.g.:
{
	"isLoggedIn": true,
	"userId": "46989208783376136",
	"accessToken": "V4yShncvbYgUq4QlefWyyskULq5D7thjN_BeEvtx_P61NuhOOpajG5UYdGbgIoIm0cE6E7YImOnhvjaIyVrAdszb6vETiytC53hzAoRhZxFRyO-LRRzHMGJinYh8JJCAWwp3X52iiZe2-dEYv6Bu7g",
	"accessTokenExpires": 1529419163,
	"clientId": "46755278331571209",
	"username": "fzzrr",
	"avatars": {
		"menu_user_av_small": "https:\/\/images.gog.com\/f6eaa2495212ffee6349a26af4a044277467aca5f2505661073396f8c4a81593_menu_user_av_small.jpg",
		"menu_user_av_small2": "https:\/\/images.gog.com\/f6eaa2495212ffee6349a26af4a044277467aca5f2505661073396f8c4a81593_menu_user_av_small2.jpg",
		"menu_user_av_big": "https:\/\/images.gog.com\/f6eaa2495212ffee6349a26af4a044277467aca5f2505661073396f8c4a81593_menu_user_av_big.jpg",
		"menu_user_av_big2": "https:\/\/images.gog.com\/f6eaa2495212ffee6349a26af4a044277467aca5f2505661073396f8c4a81593_menu_user_av_big2.jpg"
	},
	"avatar": "\/\/images.gog.com\/f6eaa2495212ffee6349a26af4a044277467aca5f2505661073396f8c4a81593",
	"cacheExpires": 1529415563
}
*/
// ----
// ----
// GET: https://menu.gog.com/v1/account/friends
/* >
{
	"friends": [
		{
			"username": "ukindom",
			"userId": "49666495027082883",
			"avatar": "\/\/images.gog.com\/3f9e109ac09308f7d52c607c8571e63d5fb482acca499a83e767dfff7f00d57d"
		},
		{
			"username": "Yanrishatum",
			"userId": "48080757671660609", // :GalaxyUserID
			"avatar": "\/\/images.gog.com\/8a14f1664d76b51f43b3fecaaa89aa7651dab4c7dab2635d951e02390642fb50"
		},
		{
			"username": "eugene20237",
			"userId": "50611737516861704",
			"avatar": "\/\/images.gog.com\/b22b5e1aa66ce2027d57e140e28bc5c088c28b3ad523aa8108a0b842f1264003"
		}
	]
}
*/


pub mod notifications;


use std::borrow::Cow;
use net::Request;
use net::PROTOCOL;
use user::GalaxyUserID;
use traits::IClient;


const HOST: &'static str = "menu.gog.com";


// TODO: rename
/// Get token to access social features like notifications.
pub struct MenuAuth();
impl Request for MenuAuth {
	type Body = ();
	type Response = self::ds::MenuAuthResponse;

	fn uri<'a, C: IClient>(&self, _client: &C) -> Cow<'a, str> {
		// https://menu.gog.com/v1/account/basic?locale=en-US&currency=RUB&country=RU
		let query = "locale=en-US&currency=RUB&country=RU";
		format!("{}://{}/v1/account/basic?{}", PROTOCOL, HOST, query).into()
	}
}


// TODO: auth by header?
pub struct Friends<'t>(pub &'t self::ds::SocialToken);
impl<'t> Request for Friends<'t> {
	type Body = ();
	type Response = self::ds::FriendsList;

	fn uri<'a, C: IClient>(&self, _client: &C) -> Cow<'a, str> {
		// https://menu.gog.com/v1/account/friends
		format!("{}://{}/v1/account/friends", PROTOCOL, HOST).into()
	}
}


pub mod ds {
	use super::*;
	use chrono::UTC;
	use app::TokenState;


	// TODO: rename
	#[derive(Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct MenuAuthResponse {
		#[serde(flatten)]
		pub token: SocialToken,
		#[serde(flatten)]
		pub user: MenuUserInfo,
		// pub username: String,
		// pub is_logged_in: bool,
		// pub avatars: BasicMenuAvatars,
		// pub avatar: String,
		pub cache_expires: i64,
	}

	#[cfg_attr(feature = "cffi", repr(C))]
	#[cfg_attr(feature = "cffi", no_mangle)]
	#[derive(Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct MenuUserInfo {
		pub username: String,
		pub is_logged_in: bool,
		pub avatars: BasicMenuAvatars,
		pub avatar: String,
	}

	#[cfg_attr(feature = "cffi", repr(C))]
	#[cfg_attr(feature = "cffi", no_mangle)]
	#[derive(Serialize, Deserialize, Debug)]
	pub struct BasicMenuAvatars {
		pub menu_user_av_small: String,
		pub menu_user_av_small2: String,
		pub menu_user_av_big: String,
		pub menu_user_av_big2: String,
	}

	#[cfg_attr(feature = "cffi", repr(C))]
	#[derive(Serialize, Deserialize, Debug, Clone)]
	#[serde(rename_all = "camelCase")]
	pub struct SocialToken {
		pub user_id: GalaxyUserID,
		pub client_id: String, // TODO: отдельная struct `SocialClientID` like for UserID
		pub access_token: String,
		pub access_token_expires: i64,

		#[serde(default = "timestamp")]
		pub timestamp: i64,
	}

	impl SocialToken {
		pub fn expired(&self) -> bool {
			let now = UTC::now().timestamp();
			now >= self.access_token_expires
		}

		pub fn state(t: &Option<&Self>) -> TokenState {
			use app::TokenState::*;
			match t {
				Some(token) => {
					if token.expired() {
						Expired
					} else {
						Normal
					}
				},
				None => Missed,
			}
		}
	}

	#[cfg_attr(feature = "cffi", repr(C))]
	#[derive(Serialize, Deserialize, Debug)]
	pub struct FriendsList {
		pub friends: Vec<Friend>,
	}

	#[cfg_attr(feature = "cffi", repr(C))]
	#[derive(Serialize, Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct Friend {
		pub user_id: GalaxyUserID,
		pub username: String,
		pub avatar: String,
	}


	fn timestamp() -> i64 { UTC::now().timestamp() }
}


// ALSO:
// https://presence.gog.com/statuses?user_id=50611737516861704,49666495027082883,48080757671660609
/* all friends offline:
{
    "total_count": 0,
    "limit": 250,
    "items": []
}
*/
/* one friend online:
{
    "total_count": 1,
    "limit": 250,
    "items": [
        {
            "data": {},
            "user_id": "49666495027082883",
            "client_id": "46755278331571209"
        }
    ]
}
*/
