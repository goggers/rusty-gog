// https://notifications.gog.com/clients/46755278331571209/users/46989208783376136/notifications?types=chat,friends,user_notification,activity_feed&access_token=V4yShncvbYgUq4QlefWyyskULq5D7thjN_BeEvtx_P61NuhOOpajG5UYdGbgIoIm0cE6E7YImOnhvjaIyVrAdszb6vETiytC53hzAoRhZxFRyO-LRRzHMGJinYh8JJCAWwp3X52iiZe2-dEYv6Bu7g
/*
{
    "items": []
}
*/

// запрос:
// OPTIONS, then PUT
// https://notifications.gog.com/clients/46755278331571209/users/46989208783376136/notifications/6568793329246583811?access_token=V4yShncvbYgUq4QlefWyyskULq5D7thjN_BeEvtx_P61NuhOOpajG5UYdGbgIoIm0cE6E7YImOnhvjaIyVrAdszb6vETiytC53hzAoRhZxFRyO-LRRzHMGJinYh8JJCAWwp3X52iiZe2-dEYv6Bu7g
// Access-Control-Request-Headers: content-type
// ответ < OPTIONS:
/*
Access-Control-Allow-Methods: PUT,DELETE
Content-Length: 20
*/
// запрос: PUT:
// Access-Control-Request-Headers: content-type
// Access-Control-Request-Method: PUT
/*
MimeType: application/json
Encoding: UTF-8
Data (17 B) :
{"consumed":true}
*/
// ответ < PUT:
/*
Access-Control-Allow-Methods: PUT,DELETE
Content-Length: 20
*/

// +header: Accept: application/json, text/plain
// Protocol: SPDY/3.1
/* < GET
// Access-Control-Allow-Methods: GET,DELETE
{
    "items": [
        {
            "id": "6568793329246583811",
            "consumed": true,
            "type": "chat",
            "payload": "{\u0022data\u0022:{\u0022id\u0022:\u0022316089364\u0022,\u0022from_user_id\u0022:\u002249666495027082883\u0022,\u0022sender_id\u0022:\u002249666495027082883\u0022,\u0022sender_username\u0022:\u0022ukindom\u0022,\u0022room_id\u0022:\u002250248860255312593\u0022,\u0022publish_date\u0022:\u00222018-06-19T13:52:57+00:00\u0022,\u0022message_content\u0022:\u0022Cze\\u015b\\u0107\u0022,\u0022images\u0022:{\u0022medium\u0022:\u0022https:\\\/\\\/images.gog.com\\\/3f9e109ac09308f7d52c607c8571e63d5fb482acca499a83e767dfff7f00d57d_avm.jpg\u0022,\u0022medium_2x\u0022:\u0022https:\\\/\\\/images.gog.com\\\/3f9e109ac09308f7d52c607c8571e63d5fb482acca499a83e767dfff7f00d57d_avm2.jpg\u0022}},\u0022key\u0022:\u0022newChatMessage\u0022}",
            "date_created": "2018-06-19T13:52:58+0000",
            "date_updated": "2018-06-19T13:52:58+0000",
            "date_expiration": "2019-06-14T13:52:58+0000",
            "unique_key": "chat_message-49666495027082883",
            "counter": 1,
            "date_consumed": "2018-06-19T14:07:50+0000"
        }
    ]
}
*/


use net::RequestBody;
use std::fmt;
use std::io::Read;
use std::borrow::Cow;
use net::Request;
use net::Method;
use net::PROTOCOL;
use user::UserID;
use user::GalaxyUserID;
use traits::IClient;


pub(crate) const HOST: &str = "notifications.gog.com";


// pub struct Notifications();
// impl Request for Notifications {
// 	type Response = ::serde_json::Value;
// 	fn uri<'a>(&self, _client: &Client) -> Cow<'a, str> {
// 		// HOST/clients/{my clientId from menu}/users/my GalaxyUserID/notifications
// 		// ?types=chat,friends,user_notification,activity_feed&access_token={TokenFromMenu}
// 		//
// 		format!(
// 		        "{proto}://{host}/{uri}",
// 		        proto = PROTOCOL,
// 		        host = HOST,
// 		        uri = "USED_DATA_GAMES"
// 		).into()
// 	}
// }


pub struct GetNotifications();
impl Request for GetNotifications {
	type Body = ();
	type Response = self::ds::NotificationsList;
	const METHOD: Method = Method::GET;

	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		// https://notifications.gog.com/clients/46755278331571209/users/46989208783376136/notifications?types=chat,friends,user_notification,activity_feed&access_token=jRBdqkmtoPNH6RhGRd0sS5Y3MUJ32L0lXnrLEqYpQdDUATPfUUxlMKj5rujNErvRjApB6igJ7pDHjDedsGHnftj6vWvLwtPrRvCPgJ8i8hpU-VYFc32pL1TziQAE4y1yeY_s3fPMxsR2ZvRA5222_g
		let token = if let Some(token) = client.social_token() {
			token
		} else {
			panic!("No social token :(")
		};
		let query = format!(
		                    "types=chat,friends,user_notification,activity_feed&access_token={token}",
		                    token = token.access_token
		);
		format!(
		        "{}://{}/clients/{client}/users/{user}/notifications?{query}",
		        PROTOCOL,
		        HOST,
		        client = token.client_id,
		        user = token.user_id,
		        query = query
		).into()
	}
}

fn uri_put_notification_status<'a, C: IClient>(client: &C, nid: &NotificationID) -> Cow<'a, str> {
	let token = if let Some(token) = client.social_token() {
		token
	} else {
		panic!("No social token :(")
	};
	let query = format!("access_token={token}", token = token.access_token);
	format!(
	        "{}://{}/clients/{client}/users/{user}/notifications/{nid}?{query}",
	        PROTOCOL,
	        HOST,
	        client = token.client_id,
	        user = token.user_id,
	        nid = nid,
	        query = query
	).into()
}


// TODO: ...
// OPTIONS with all like for SetNotificationStatus
pub struct CanSetNotificationStatus<'n>(pub &'n NotificationID);
impl<'n> Request for CanSetNotificationStatus<'n> {
	type Body = ();
	type Response = ();
	const METHOD: Method = Method::OPTIONS;
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> { uri_put_notification_status(client, self.0) }

	// fn deserialize_response<R: Read>(mut response: R) -> net::Result<Self::Response>
	// 	where Self::Response: ::request::Deserialize {
	// 	Ok(())
	// }
}

// impl<'n> ::net::ResponseDeserializer for CanSetNotificationStatus<'n> {
// 	type Response = ();
// 	fn deserialize_response<R: Read>(mut response: R) -> ::net::Result<Self::Response>
// 		where Self::Response: ::net::DeserializeOwned {
// 		Ok(())
// 	}
// }


// PUT with:
// Access-Control-Request-Headers: content-type
// Access-Control-Request-Method: PUT
// body: `{"consumed":true}`
pub struct SetNotificationStatus<'n, 'b>(pub &'n NotificationID, pub &'b self::ds::SetNotificationStatusBody);
impl<'n, 'b> Request for SetNotificationStatus<'n, 'b> {
	type Body = self::ds::SetNotificationStatusBody;
	type Response = ();
	const METHOD: Method = Method::PUT;
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> { uri_put_notification_status(client, self.0) }
	// fn body(&self, _client: &Client) -> Option<Self::Body> { Some(self.1.clone()) }
}
impl<'n, 'b> RequestBody<self::ds::SetNotificationStatusBody> for SetNotificationStatus<'n, 'b> {
	fn body<Client: IClient>(&self, _client: &Client) -> Option<self::ds::SetNotificationStatusBody> { Some(self.1.clone()) }
}
// impl<'n, 'b> RequestBody for SetNotificationStatus<'n, 'b> {
// 	type Body = self::ds::SetNotificationStatusBody;
// 	fn body(&self, _client: &Client) -> Option<Self::Body> { Some(self.1.clone()) }
// }


// OPTIONS with `Access-Control-Request-Method: DELETE`
pub struct CanDeleteNotification<'t>(pub &'t ::social::ds::SocialToken);
// DELETE with `Access-Control-Request-Method: DELETE` & body: `{}`
pub struct DeleteNotification<'t>(pub &'t ::social::ds::SocialToken);


pub mod ds {
	use super::*;
	use chrono::UTC;
	use serde::{Deserialize, Deserializer};


	#[derive(Serialize, Deserialize, Debug)]
	pub struct NotificationsList {
		pub items: Vec<Notification>,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct Notification {
		pub id: NotificationID,
		pub consumed: bool,
		#[serde(rename = "type")]
		pub kind: String, // TODO: enum: e.g.:chat
		#[serde(deserialize_with = "deserialize_notification_payload")]
		pub payload: NotificationPayload,
		// datetimes:
		pub date_created: String,
		pub date_updated: String,
		pub date_expiration: String,
		pub date_consumed: String,
		// ...
		pub unique_key: String,
		pub counter: u32,
	}


	#[derive(Serialize, Deserialize, Debug)]
	pub struct NotificationPayload {
		pub data: ::account::chat::ds::ChatMessagePayload,
		pub key: String, // TODO: enum "newChatMessage"
	}


	// fn deserialize_notification_payload_key<'de, D>(deserializer: D) -> Result<NotificationPayloadKey, D::Error>
	fn deserialize_notification_payload<'de, D>(deserializer: D) -> Result<NotificationPayload, D::Error>
		where D: Deserializer<'de> {
		use serde::de::Error;
		let json = String::deserialize(deserializer)?;
		::serde_json::de::from_str::<NotificationPayload>(&json).map_err(Error::custom)
	}


	#[derive(Serialize, Deserialize, Debug, Clone)]
	pub struct SetNotificationStatusBody {
		pub consumed: bool,
	}
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct NotificationID(pub String);
impl<T: AsRef<str>> From<T> for NotificationID {
	fn from(value: T) -> Self { NotificationID(value.as_ref().to_string()) }
}
impl<'s> Into<&'s str> for &'s NotificationID {
	fn into(self) -> &'s str { &self.0 }
}
impl fmt::Display for NotificationID {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{}", self.0) }
}
