///! Describes all public info about other users relative logged-in current user.
use std::borrow::Cow;
use std::fmt;
use net::Request;
use net::PROTOCOL;
use traits::IClient;


/// Any user profile + relations to logged-in current user.
pub struct UserInfo(pub UserID, pub Option<ds::ExtraQuery>);
impl Request for UserInfo {
	type Body = ();
	type Response = self::ds::UserInfo;
	fn uri<'a, C: IClient>(&self, _client: &C) -> Cow<'a, str> {
		const HOST: &'static str = "embed.gog.com";
		let query = match &self.1 {
			Some(query) => format!("?{}", query),
			None => Default::default(),
		};
		format!(
		        "{proto}://{host}/users/info/{id}{query}",
		        proto = PROTOCOL,
		        host = HOST,
		        id = self.0,
		        query = query
		).into()
	}
}

// https://gogapidocs.readthedocs.io/en/latest/galaxy.html#users-gog-com
pub struct PubUserInfo(pub GalaxyUserID, pub Option<ds::ExtraQuery>);
impl Request for PubUserInfo {
	type Body = ();
	type Response = self::ds::PubUserInfo;
	fn uri<'a, C: IClient>(&self, _client: &C) -> Cow<'a, str> {
		const HOST: &'static str = "users.gog.com";
		let query = match &self.1 {
			Some(query) => format!("?{}", query),
			None => Default::default(),
		};
		format!(
		        "{proto}://{host}/users/{id}{query}",
		        proto = PROTOCOL,
		        host = HOST,
		        id = self.0,
		        query = query
		).into()
	}
}


pub mod ds {
	use super::*;
	use std::fmt;
	use serde::{Serialize, Serializer};
	use serde::{Deserialize, Deserializer};

	#[derive(Serialize, Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct UserInfo {
		pub id: UserID,
		pub galaxy_user_id: Option<GalaxyUserID>,
		pub username: String,
		/// timestamp
		pub user_since: i64,

		pub avatars: UserInfoAvatars,
		pub friend_status: Option<UserFriendStatus>,
		pub wishlist_status: Option<UserWishlistStatus>,
		pub blocked_status: Option<UserBlockStatus>,
		pub chat_status: UserChatStatus,
	}

	#[derive(Serialize, Deserialize, Debug)]
	// #[serde(rename_all = "camelCase")]
	pub struct PubUserInfo {
		pub id: GalaxyUserID,
		pub username: String,
		/// timestamp/datetime e.g.: "2015-12-04T14:02:42+00:00"
		pub created_date: String,

		pub avatar: GogImage,
		pub is_employee: bool,
		pub tags: Vec<::serde_json::Value>, // TODO:

		/// only for current user (my acc)
		pub email: Option<String>,
		/// only for current user (my acc)
		pub roles: Option<Vec<::serde_json::Value>>, // TODO: enum
		/// only for current user (my acc)
		pub status: Option<u8>, // TODO: enum
		/// only for current user (my acc)
		pub checksum: Option<String>,
		/// only for current user (my acc)
		pub settings: Option<::serde_json::Value>, // {allow_to_be_invited_by:"anyone", allow_to_be_searched:bool, use_two_step_authentication:bool}
		/// only for current user (my acc)
		pub password_set: Option<bool>,
		/// only for current user (my acc)
		pub marketing_consent: Option<bool>,
	}


	#[derive(Serialize, Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct UserFriendStatus {
		pub id: UserID,
		/// https://gogapidocs.readthedocs.io/en/latest/account.html#get--users-info-(int-user_id)
		pub status: FriendStatus,
		/// Timestamp of when a friend request was sent or null.
		pub date_created: Option<i64>,
		/// Timestamp of when a friend request was accepted or null.
		pub date_accepted: Option<i64>,
	}

	#[repr(u8)]
	#[derive(Debug, Clone, Copy)]
	pub enum FriendStatus {
		/// No special relationship with this user.
		AnonymousUser = 0_u8,
		/// You have sent this user a friend request.
		InvitedUser = 1,
		/// You have received a friend request from this user.
		InvitedByUser = 2,
		/// You are friends with this user.
		Friend = 3,
	}

	impl Serialize for FriendStatus {
		fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
			where S: Serializer {
			serializer.serialize_u8(*self as u8)
		}
	}

	impl<'de> Deserialize<'de> for FriendStatus {
		fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
			where D: Deserializer<'de> {
			use std::mem::transmute;
			use de::U8Visitor;
			match deserializer.deserialize_u8(U8Visitor) {
				Ok(value) => {
					let status = unsafe { transmute::<u8, FriendStatus>(value) };
					Ok(status)
				},
				Err(error) => Err(error),
			}
		}
	}


	#[derive(Serialize, Deserialize, Debug)]
	pub struct UserWishlistStatus {
		/// https://gogapidocs.readthedocs.io/en/latest/account.html#get--users-info-(int-user_id)
		pub sharing: WishlistSharingStatus,
		pub url: String,
	}

	#[repr(u8)]
	#[derive(Debug, Clone, Copy)]
	pub enum WishlistSharingStatus {
		Private = 0_u8,
		Public = 1,
		ForFriends = 2,
	}

	impl Serialize for WishlistSharingStatus {
		fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
			where S: Serializer {
			serializer.serialize_u8(*self as u8)
		}
	}
	impl<'de> Deserialize<'de> for WishlistSharingStatus {
		fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
			where D: Deserializer<'de> {
			use std::mem::transmute;
			use de::U8Visitor;
			match deserializer.deserialize_u8(U8Visitor) {
				Ok(value) => {
					let status = unsafe { transmute::<u8, WishlistSharingStatus>(value) };
					Ok(status)
				},
				Err(error) => Err(error),
			}
		}
	}


	#[derive(Serialize, Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct UserChatStatus {
		pub is_chat_restricted: bool,
		pub url: String,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct UserBlockStatus {
		pub blocked: bool,
	}


	pub struct ExtraQuery {
		pub friend_status: bool,
		pub wishlist_status: bool,
		pub blocked_status: bool,
	}

	impl Default for ExtraQuery {
		fn default() -> Self {
			Self { friend_status: true,
			       wishlist_status: true,
			       blocked_status: true, }
		}
	}

	impl fmt::Display for ExtraQuery {
		fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
			let mut result = Vec::new();
			const DELIMITER: &str = ",";
			if self.friend_status {
				result.push("friendStatus");
			}

			if self.wishlist_status {
				result.push("wishlistStatus");
			}

			if self.blocked_status {
				result.push("blockedStatus");
			}

			write!(
			       f,
			       "{}",
			       if result.len() > 2 {
				       format!("expand={}", result.join(DELIMITER))
				      } else {
				       "".to_owned()
				      }
			)
		}
	}


	#[derive(Serialize, Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct UserInfoAvatars {
		pub small: Option<String>,
		pub small2x: Option<String>,
		pub medium: Option<String>,
		pub medium2x: Option<String>,
		pub large: Option<String>,
		pub large2x: Option<String>,
	}


	#[derive(Serialize, Deserialize, Debug)]
	pub struct GogImage {
		pub gog_image_id: String,
		pub small: String,
		pub small_2x: String,
		pub medium: String,
		pub medium_2x: String,
		pub large: String,
		pub large_2x: String,

		pub sdk_img_32: String,
		pub sdk_img_64: String,
		pub sdk_img_184: String,

		pub menu_small: String,
		pub menu_small_2: String,
		pub menu_big: String,
		pub menu_big_2: String,
	}
}


/// e.g.: `48628349971017`.
/// cbindgen:field-names=[value]
#[cfg_attr(feature = "cffi", repr(C))]
#[cfg_attr(feature = "cffi", no_mangle)]
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct UserID(pub String);
impl<T: AsRef<str>> From<T> for UserID {
	fn from(value: T) -> Self { UserID(value.as_ref().to_string()) }
}
impl<'s> Into<&'s str> for &'s UserID {
	fn into(self) -> &'s str { &self.0 }
}
impl fmt::Display for UserID {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{}", self.0) }
}


/// e.g.: `50611737516861704`.
/// cbindgen:field-names=[value]
#[cfg_attr(feature = "cffi", repr(C))]
#[cfg_attr(feature = "cffi", no_mangle)]
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GalaxyUserID(pub String);
impl<T: AsRef<str>> From<T> for GalaxyUserID {
	fn from(value: T) -> Self { GalaxyUserID(value.as_ref().to_string()) }
}

// impl<'s> From<&'s str> for GalaxyUserID {
// 	fn from(value: &str) -> Self { GalaxyUserID(value.to_owned()) } }

impl<'s> Into<&'s str> for &'s GalaxyUserID {
	fn into(self) -> &'s str { &self.0 }
}

impl fmt::Display for GalaxyUserID {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{}", self.0) }
}
