use std::fmt;
use cfg::ds::Config;
use traits::{IClient, IClientMut};
use auth::ds::AuthToken;
use social::ds::SocialToken;


#[cfg_attr(feature = "cffi", repr(C))]
#[derive(Serialize, Deserialize, Debug)]
pub enum Platform {
	Desktop,
}

impl fmt::Display for Platform {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{}", match self {
			Platform::Desktop => "desktop",
		})
	}
}

#[cfg_attr(feature = "cffi", repr(C))]
#[derive(Serialize, Deserialize, Debug)]
pub enum Project {
	Galaxy,
}

impl fmt::Display for Project {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{}", match self {
			Project::Galaxy => "galaxy",
		})
	}
}

#[cfg_attr(feature = "cffi", repr(C))]
#[derive(Serialize, Deserialize, Debug)]
pub enum Kind {
	Client,
	Commservice,
	Overlay,
	Peer,
	Updater,
}

impl fmt::Display for Kind {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "{}", match self {
			Kind::Client => "client",
			Kind::Commservice => "commservice",
			Kind::Overlay => "overlay",
			Kind::Peer => "peer",
			Kind::Updater => "updater",
		})
	}
}


#[cfg_attr(feature = "cffi", repr(C))]
/// Describes the type of client.
#[derive(Serialize, Deserialize, Debug)]
pub struct Application {
	platform: Platform,
	project: Project,
	kind: Kind,
}

/// to something like `desktop-galaxy-client`
impl fmt::Display for Application {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{}-{}-{}", self.platform, self.project, self.kind) }
}

impl Default for Application {
	/// creates Desktop Galaxy Client.
	fn default() -> Self {
		Self { kind: Kind::Client,
		       project: Project::Galaxy,
		       platform: Platform::Desktop, }
	}
}


#[derive(Serialize, Deserialize, Debug)]
pub struct Client {
	application: Application,
	/// current version
	descriptor: Descriptor,
	config: Option<Config>,
	/// main auth token
	auth_token: Option<AuthToken>,
	/// social token used for notifications
	social_token: Option<SocialToken>,
}

impl Client {
	pub fn application(&self) -> &Application { &self.application }
	pub fn descriptor(&self) -> &Descriptor { &self.descriptor }

	pub fn config_mut(&mut self) -> &mut Option<Config> { &mut self.config }
	pub fn config(&self) -> Option<&Config> {
		match &self.config {
			Some(res) => Some(&res),
			None => None,
		}
	}

	pub fn auth_token_mut(&mut self) -> &mut Option<AuthToken> { &mut self.auth_token }
	pub fn auth_token(&self) -> Option<&AuthToken> {
		match &self.auth_token {
			Some(res) => Some(&res),
			None => None,
		}
	}

	pub fn social_token_mut(&mut self) -> &mut Option<SocialToken> { &mut self.social_token }
	pub fn social_token(&self) -> Option<&SocialToken> {
		match &self.social_token {
			Some(res) => Some(&res),
			None => None,
		}
	}

	pub fn auth_token_state(&self) -> TokenState {
		use self::TokenState::*;
		return match &self.auth_token {
			Some(token) => {
				if token.expired() {
					Expired
				} else {
					Normal
				}
			},
			None => Missed,
		};
	}

	pub fn social_token_state(&self) -> TokenState {
		use self::TokenState::*;
		return match &self.social_token {
			Some(token) => {
				if token.expired() {
					Expired
				} else {
					Normal
				}
			},
			None => Missed,
		};
	}
}


impl Default for Client {
	/// creates Desktop Galaxy Client.
	fn default() -> Self {
		Self { config: None,
		       auth_token: None,
		       social_token: None,
		       descriptor: Descriptor::default(),
		       application: Application::default(), }
	}
}

// #[cfg_attr(feature = "cffi", no_mangle)]
#[cfg_attr(feature = "cffi", repr(C))]
/// Describes the build & key of the current client.
#[derive(Serialize, Deserialize, Debug)]
pub struct Descriptor {
	pub id: String,
	pub secret: String,
	/// Url-schema & URL.
	/// Used as redirect-uri for auth.
	pub uri: String,
}

impl Default for Descriptor {
	fn default() -> Self { Self::galaxy() }
}

impl Descriptor {
	pub fn galaxy() -> Self {
		Self { id: String::from("46899977096215655"),
		       secret: String::from("9d85c43b1482497dbbce61f6e4aa173a433796eeae2ca8c5f6129f2dc4de46d9"),
		       uri: String::from("https://embed.gog.com/on_login_success?origin=client"), }
	}
}


#[derive(Debug)]
#[cfg_attr(feature = "cffi", repr(C))]
pub enum TokenState {
	Normal,
	Expired,
	Missed,
}


impl IClient for Client {
	fn application(&self) -> &Application { self.application() }
	fn descriptor(&self) -> &Descriptor { self.descriptor() }
	fn config(&self) -> Option<&Config> { self.config() }
	fn auth_token(&self) -> Option<&AuthToken> { self.auth_token() }
	fn social_token(&self) -> Option<&SocialToken> { self.social_token() }
	fn auth_token_state(&self) -> TokenState { self.auth_token_state() }
	fn social_token_state(&self) -> TokenState { self.social_token_state() }
}

impl IClientMut for Client {
	fn config_mut(&mut self) -> &mut Option<Config> { &mut self.config }
	fn auth_token_mut(&mut self) -> &mut Option<AuthToken> { &mut self.auth_token }
	fn social_token_mut(&mut self) -> &mut Option<SocialToken> { &mut self.social_token }
}
