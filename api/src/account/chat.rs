use net::RequestBody;
use std::fmt;
use std::io::Read;
use std::borrow::Cow;
use net::PROTOCOL;
use net::Request;
use net::Method;
use user::GalaxyUserID;
use user::ds::UserInfoAvatars;
use traits::IClient;


/// default host
const HOST: &str = "chat.gog.com";


/// Only if authed user or who share friends list.
/// inner: GalaxyUserID
pub struct GetFriendsList(pub GalaxyUserID);
impl Request for GetFriendsList {
	type Body = ();
	type Response = ds::FriendsList;

	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!(
		        "{proto}://{host}/users/{id}/friends",
		        proto = PROTOCOL,
		        host = HOST,
		        id = self.0
		).into()
	}
}

/// Only for authed user.
/// inner: GalaxyUserID
pub struct GetInvitationsList(pub GalaxyUserID);
impl Request for GetInvitationsList {
	type Body = ();
	type Response = ds::FriendsList;

	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!(
		        "{proto}://{host}/users/{id}/invitations",
		        proto = PROTOCOL,
		        host = HOST,
		        id = self.0
		).into()
	}
}


// TODO: TEST ME
/// Marks the unread message as read.
pub struct ReadTheUnreadMessage<'m>(pub GalaxyUserID, pub &'m ds::ChatMessagePayload);
impl<'m> Request for ReadTheUnreadMessage<'m> {
	type Body = ();
	type Response = ::serde_json::Value;
	const METHOD: Method = Method::DELETE;

	// https://chat.gog.com/users/46989208783376136/rooms/50248860255312593/unread_messages/316095298
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!(
		        "{proto}://{host}/users/{id}/rooms/{room_id}/unread_messages/{msg_id}",
		        proto = PROTOCOL,
		        host = HOST,
		        id = self.0,
		        room_id = self.1.room_id,
		        msg_id = self.1.id
		).into()
	}
}

// https://chat.gog.com/users/46989208783376136/rooms
pub struct RoomsList(pub GalaxyUserID);
impl Request for RoomsList {
	type Body = ();
	type Response = ds::RoomsList;
	const METHOD: Method = Method::GET;

	// https://chat.gog.com/users/46989208783376136/rooms/50248860255312593/unread_messages/316095298
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!(
		        "{proto}://{host}/users/{id}/rooms",
		        proto = PROTOCOL,
		        host = HOST,
		        id = self.0
		).into()
	}
}


// POST:  https://chat.gog.com/users/46989208783376136/rooms/50248860255312593/messages
/*  Content-Type: application/json;charset=UTF-8
>>
{"content":"ping"}
<<
{
    "id": "316298521",
    "content": "ping",
    "date": "2018-06-21T17:57:13+00:00",
    "type": "chat_message",
    "from": {
        "user_id": "46989208783376136",
        "username": "fzzrr",
        "is_employee": false,
        "images": {
            "medium": "https:\/\/images.gog.com\/f6eaa2495212ffee6349a26af4a044277467aca5f2505661073396f8c4a81593_avm.jpg",
            "medium_2x": "https:\/\/images.gog.com\/f6eaa2495212ffee6349a26af4a044277467aca5f2505661073396f8c4a81593_avm2.jpg"
        }
    }
}
*/

// TODO: DELETE /users/46989208783376136/rooms/50248860255312593/messages HTTP/1.1
// clear conversation


pub struct PostMessageTo(pub GalaxyUserID, pub ChatRoomID, pub ds::PostMessageBody);
impl Request for PostMessageTo {
	type Body = ds::PostMessageBody;
	type Response = ds::Message;
	// type Response = ::serde_json::Value;
	const METHOD: Method = Method::POST;

	// https://chat.gog.com/users/46989208783376136/rooms/50248860255312593/messages
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!(
		        "{proto}://{host}/users/{id}/rooms/{room_id}/messages",
		        proto = PROTOCOL,
		        host = HOST,
		        id = self.0,
		        room_id = self.1
		).into()
	}

	// fn body(&self, _client: &Client) -> Option<Self::Body> { Some(self.2.clone()) }

	// fn deserialize_response<R: Read>(mut response: R) -> ::request::Result<Self::Response>
	// 	where Self::Response: ::request::Deserialize {
	// 	Ok(())
	// }
}
impl RequestBody<ds::PostMessageBody> for PostMessageTo {
	fn body<Client: IClient>(&self, _client: &Client) -> Option<ds::PostMessageBody> { Some(self.2.clone()) }
}


/// (My GalaxyUserID, RoomID, {after message:ID})
pub struct GetMessages(pub GalaxyUserID, pub ChatRoomID, pub Option<MessageID>);
impl Request for GetMessages {
	type Body = ds::PostMessageBody;
	type Response = ds::MessagesList;
	const METHOD: Method = Method::GET;

	// https://chat.gog.com/users/46989208783376136/rooms/50248860255312593/messages ?after_message=317012374
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		let query = match &self.2 {
			Some(id) => format!("?after_message={}", id),
			None => Default::default(),
		};
		format!(
		        "{proto}://{host}/users/{id}/rooms/{room_id}/messages{query}",
		        proto = PROTOCOL,
		        host = HOST,
		        id = self.0,
		        room_id = self.1,
		        query = query,
		).into()
	}
}


// https://chat.gog.com/users/46989208783376136/friends_main
pub struct FriendsMain(pub GalaxyUserID);
impl Request for FriendsMain {
	type Body = ();
	type Response = ds::FriendsList;
	const METHOD: Method = Method::GET;

	// https://chat.gog.com/users/46989208783376136/rooms/50248860255312593/unread_messages/316095298
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!(
		        "{proto}://{host}/users/{id}/friends_main",
		        proto = PROTOCOL,
		        host = HOST,
		        id = self.0
		).into()
	}
}


// GET:   https://chat.gog.com/users/46989208783376136/rooms/50248860255312593/messages
// [ ?after_message=316095298 ]
pub struct RoomMessagesList(pub GalaxyUserID, pub ChatRoomID);
impl Request for RoomMessagesList {
	type Body = ();
	type Response = ds::MessagesList;
	const METHOD: Method = Method::GET;

	// https://chat.gog.com/users/46989208783376136/rooms/50248860255312593/messages
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!(
		        "{proto}://{host}/users/{id}/rooms/{room_id}/messages",
		        proto = PROTOCOL,
		        host = HOST,
		        id = self.0,
		        room_id = self.1
		).into()
	}
}


// TODO: GET: https://chat.gog.com/users/46989208783376136/friends/49666495027082883
// Authorization: Bearer 8TbMreigiwYF1aO2PMrA9hwik0g...
/*
{
    "user_id": "49666495027082883",
    "username": "ukindom",
    "is_employee": false,
    "friend_status": "friend",
    "images": {
        "medium": "https:\/\/images.gog.com\/3f9e109ac09308f7d52c607c8571e63d5fb482acca499a83e767dfff7f00d57d_avm.jpg",
        "medium_2x": "https:\/\/images.gog.com\/3f9e109ac09308f7d52c607c8571e63d5fb482acca499a83e767dfff7f00d57d_avm2.jpg"
    }
}
*/


// GET: https://chat.gog.com/users/46989208783376136/chat?access_token=8TbMreigiwYF1aO2PMrA9hwik0glO0P3ZO1of2AT2wssYAyu6dh6E795lW-qz4hcj85ZIYmGIBtF6RRzwJtdfxRPGB4vU3MC65XJVEjiV28PE3eBzZPrUQhb7eGxm4u447JTClFpczLXCbvztWrGIA


pub mod ds {
	use super::*;
	// use serde::Serialize;
	// use serde::Deserialize;
	// use serde::de::DeserializeOwned;


	#[derive(Serialize, Deserialize, Debug)]
	pub struct FriendsList {
		pub items: Vec<Friend>,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct Friend {
		pub user_id: GalaxyUserID,
		pub username: String,
		pub is_employee: bool,
		pub friend_status: String, // TODO: enum with ="friend"
		pub images: UserInfoAvatars,
		/// __only when in the `ChatFriendsList`__
		pub room: Option<Room>,
	}


	#[derive(Serialize, Deserialize, Debug)]
	/// List of friends __with chat rooms__.
	pub struct ChatFriendsList {
		pub items: Vec<Friend>,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct RoomsList {
		pub items: Vec<Room>,
		pub total_count: u32,
	}


	#[derive(Serialize, Deserialize, Debug)]
	pub struct Room {
		pub id: ChatRoomID,
		pub participants: Vec<GalaxyUserID>,
		pub second_participant: GalaxyUserID,
		/// Name of the room
		pub name: String,
		pub last_active: String,
		pub unread_messages_count: u32,
		pub is_employee: bool,
		pub participant_banned: bool,
		pub state: String, // TODO: enum e.g.: "visible"
	}


	#[derive(Serialize, Deserialize, Debug)]
	// Used in Notifications
	pub struct ChatMessagePayload {
		pub id: String,
		pub from_user_id: GalaxyUserID,
		pub sender_id: GalaxyUserID,
		pub sender_username: String,
		pub room_id: ChatRoomID,
		// datetime:
		pub publish_date: String,
		// content:
		pub message_content: String,
		// avatars:
		pub images: ::serde_json::Value,
	}


	#[derive(Serialize, Deserialize, Debug)]
	pub struct MessagesList {
		pub items: Vec<Message>,
		pub total_count: u32,
		pub limit: u32,
		pub has_permissions_to_publish: bool,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct Message {
		pub id: MessageID,

		#[serde(rename = "type")]
		pub kind: String, // TODO: enum e.g.: "chat_message"

		/// `Friend` but without `friend_status` & `room`
		pub from: Participant,
		pub date: String,
		pub content: String,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct Participant {
		pub user_id: GalaxyUserID,
		pub username: String,
		pub is_employee: bool,
		pub images: UserInfoAvatars,
	}


	#[derive(Serialize, Deserialize, Debug, Clone)]
	pub struct PostMessageBody {
		pub content: String,
	}
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ChatRoomID(pub String);
impl<T: AsRef<str>> From<T> for ChatRoomID {
	fn from(value: T) -> Self { ChatRoomID(value.as_ref().to_string()) }
}
impl<'s> Into<&'s str> for &'s ChatRoomID {
	fn into(self) -> &'s str { &self.0 }
}
impl fmt::Display for ChatRoomID {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{}", self.0) }
}


#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MessageID(pub String);
impl<T: AsRef<str>> From<T> for MessageID {
	fn from(value: T) -> Self { MessageID(value.as_ref().to_string()) }
}
// impl<'s> From<&'s str> for MessageID {
// 	fn from(value: &str) -> Self { MessageID(value.to_owned()) } }
impl<'s> Into<&'s str> for &'s MessageID {
	fn into(self) -> &'s str { &self.0 }
}
impl fmt::Display for MessageID {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{}", self.0) }
}
