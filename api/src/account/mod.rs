///! https://gogapidocs.readthedocs.io/en/latest/account.html
pub mod library;
pub mod chat;

use std::borrow::Cow;
use net::Request;
use net::PROTOCOL;
use user::UserID;
use user::GalaxyUserID;
use traits::IClient;


pub(crate) const HOST: &'static str = "embed.gog.com";
const USED_DATA: &'static str = "userData.json";


/// https://gogapidocs.readthedocs.io/en/latest/account.html#user
/// Private Profile Data
/// TODO: impl Response
pub struct AccountInfo();
impl Request for AccountInfo {
	type Body = ();
	type Response = ds::AccountInfo;
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!("{proto}://{host}/{file}", proto = PROTOCOL, host = HOST, file = USED_DATA).into()
	}
}


pub mod ds {
	use super::*;

	#[derive(Serialize, Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct AccountInfo {
		pub user_id: UserID,
		pub galaxy_user_id: Option<GalaxyUserID>,
		pub username: String,
		pub email: String,
		pub avatar: Option<String>,
		pub wallet_balance: Option<WalletBalance>,
		pub purchased_items: Option<PurchasedItems>,
		pub wishlisted_items: u32,
		pub friends: Option<Vec<Friend>>,
		pub country: String, // TODO: enum
		pub currencies: Vec<Currency>,
		pub selected_currency: Currency,
		pub preferred_language: Language,
		pub rating_brand: String, // TODO: enum
		pub is_logged_in: bool,
		pub checksum: Checksum,
		pub updates: Updates,
		pub personalized_product_prices: Vec<::serde_json::Value>,
		pub personalized_series_prices: Vec<::serde_json::Value>,
		pub stretch_goals: Option<::serde_json::Value>,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct WalletBalance {
		/// same as `Currency::code`
		pub currency: String,
		pub amount: i64,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct PurchasedItems {
		pub games: i64,
		pub movies: i64,
	}

	#[derive(Serialize, Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct Friend {
		pub username: String,
		/// timestamp
		pub user_since: i64,
		pub galaxy_id: GalaxyUserID,
		pub avatar: String,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct Currency {
		pub code: String,
		pub symbol: String,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct Language {
		pub code: String,
		pub name: String,
	}

	#[derive(Serialize, Deserialize, Debug)]
	pub struct Checksum {
		pub cart: Option<String>,
		pub games: Option<String>,
		pub wishlist: Option<String>,
		pub reviews_votes: Option<String>,
		pub games_rating: Option<String>,
	}

	#[derive(Serialize, Deserialize, Debug)]
	#[serde(rename_all = "camelCase")]
	pub struct Updates {
		pub messages: u32,
		pub pending_friend_requests: u32,
		pub unread_chat_messages: u32,
		pub products: u32,
		pub forum: Option<u32>,
		pub total: u32,
	}
}
