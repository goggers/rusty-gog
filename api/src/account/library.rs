use std::borrow::Cow;
use net::Request;
use net::PROTOCOL;
use user::UserID;
use user::GalaxyUserID;
use traits::IClient;

use ::account::HOST;

pub(crate) const USED_DATA_GAMES: &'static str = "user/data/games";


pub struct OwnedGamesIdList();
impl Request for OwnedGamesIdList {
	type Body = ();
	type Response = ds::GameIdList;
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!("{proto}://{host}/{uri}", proto = PROTOCOL, host = HOST, uri = USED_DATA_GAMES).into()
	}
}


pub struct OwnedGameDetails(pub ::ds::userdata::GameId);
impl Request for OwnedGameDetails {
	type Body = ();
	type Response = ::ds::userdata::GameDetails;
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!(
		        "{proto}://{host}/account/gameDetails/{content_id}.json",
		        proto = PROTOCOL,
		        host = HOST,
		        content_id = self.0
		).into()
	}
}

pub struct OwnedMovieDetails(pub ::ds::userdata::GameId);
impl Request for OwnedMovieDetails {
	type Body = ();
	// TODO: ::MovieDetails
	type Response = ::ds::userdata::GameDetails;
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		format!(
		        "{proto}://{host}/account/movieDetails/{content_id}.json",
		        proto = PROTOCOL,
		        host = HOST,
		        content_id = self.0
		).into()
	}
}


/// Searches for products **owned by the user** matching the given criterias.
/// Movies don’t support the parameters `category`, `feature`, `system`.
pub struct SearchOwnedProducts<Q: AsRef<str>,
 S: AsRef<[::search::System]>,
 G: AsRef<[::search::Genre]>,
 F: AsRef<[::search::Feature]>,
 T: AsRef<[::search::TagId]>>(pub ::search::LibraryQueryFilteredProducts<Q, S, G, F, T>,
                              pub Option<::search::PaginationQuery>);
impl<Q, S, G, F, T> Request for SearchOwnedProducts<Q, S, G, F, T>
	where Q: AsRef<str>,
	      T: AsRef<[::search::TagId]>,
	      G: AsRef<[::search::Genre]>,
	      F: AsRef<[::search::Feature]>,
	      S: AsRef<[::search::System]>
{
	type Body = ();
	// type Response = ::serde_json::Value;
	type Response = ::search::PaginationResponsePart;
	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str> {
		use search::AsQuery;
		let query: String =
			[self.0.as_query(), match &self.1 {
				Some(query) => query.as_query(),
				None => Default::default(),
			}].join("&");

		format!(
		        "{proto}://{host}//account/getFilteredProducts?{query}",
		        proto = PROTOCOL,
		        host = HOST,
		        query = query
		).into()
	}
}


pub mod search {
	use super::*;


	// ?language=,,,
	// pub struct Languages {
	// 	pub deutsch: bool,// de
	// 	// TODO: continue list
	// }

	// fn getFilteredProducts<'a>(query: &GetFilteredProducts, client: &Client) -> Cow<'a, str> {
	// 	// https://embed.gog.com/account/getFilteredProducts?hiddenFlag=0&mediaType=1&page=3&sortBy=date_purchased&totalPages=3
	// 	format!(
	// 	        "{proto}://{host}/account/{query}",
	// 	        proto = PROTOCOL,
	// 	        host = HOST,
	// 	        query = query
	// 	).into()
	// }
}


pub mod ds {
	use super::*;

	pub type GameId = ::ds::products::ProductId;

	#[derive(Serialize, Deserialize, Debug)]
	pub struct GameIdList {
		pub owned: Vec<GameId>,
	}
}
