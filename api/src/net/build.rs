#[cfg(feature = "http")]
use http::uri::InvalidUri;
use serde_json::Error as SerdeError;
use std::error::Error;
use std::fmt;
use net::Request;
use traits::IClient;
use cfg::ds::Config;
use traits::*;


pub trait RequestBuilder<T>
	where Self: Request {
	fn build<Client: IClient>(&self, client: &Client) -> Result<T, BuildError>;
}


pub trait RequestBuilderUsingParts<T>
	where Self: Request + RequestBuilder<T> {
	fn build_from<Client, Info>(&self, client: &Client, info: &Info, cfg: &Option<&Config>) -> Result<T, BuildError>
		where Info: IClientInfo,
		      Client: IClientAuth {
		self.build(&(client, info, cfg))
	}
}

// auto impl
impl<T, S> RequestBuilderUsingParts<T> for S
	where Self: Request,
	      Self: RequestBuilder<T>,
	      S: RequestBuilder<T>
{
}


#[derive(Debug)]
pub enum BuildError {
	#[cfg(feature = "http")]
	InvalidUri(InvalidUri),
	InvalidBody(SerdeError),
}

#[cfg(feature = "http")]
impl From<InvalidUri> for BuildError {
	fn from(error: InvalidUri) -> Self { BuildError::InvalidUri(error) }
}

impl From<SerdeError> for BuildError {
	fn from(error: SerdeError) -> Self { BuildError::InvalidBody(error) }
}


impl Error for BuildError {
	fn cause(&self) -> Option<&Error> {
		use self::BuildError::*;
		match self {
			#[cfg(feature = "http")]
			InvalidUri(error) => Some(error),
			InvalidBody(error) => Some(error),
		}
	}
}

impl fmt::Display for BuildError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		use self::BuildError::*;
		match self {
			#[cfg(feature = "http")]
			InvalidUri(error) => write!(f, "[GOG Internal Request Build Error: Invalid Uri: {}]", error),
			InvalidBody(error) => write!(f, "[GOG Internal Request Build Error: Invalid Body: {}]", error),
		}
	}
}
