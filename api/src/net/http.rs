use http::Request as HttpRequest;
use net::build::{RequestBuilder, BuildError};
use net::{Request, RequestBody, Method};
use traits::IClient;


impl<R, B, D> RequestBuilder<HttpRequest<D>> for R
	where R: Request<Body = B> + RequestBody<B>,
	      B: ::serde::Serialize + Clone,
	      D: From<Vec<u8>>
{
	#[inline]
	fn build<Client: IClient>(&self, client: &Client) -> Result<HttpRequest<D>, BuildError> {
		let body = match self.body(&*client) {
			Some(body) => {
				debug_assert!(Self::METHOD != Method::GET);
				::serde_json::ser::to_vec(&body)?
			},
			None => Default::default(),
		};

		// or D::from(body) is better here?
		let mut req = HttpRequest::new(body.into());
		*req.method_mut() = Self::METHOD;
		*req.uri_mut() = self.uri(&*client).parse()?;
		self.headers_to(&*client, req.headers_mut());
		Ok(req)
	}
}
