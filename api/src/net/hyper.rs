// extern crate http;
// extern crate hyper;

// use self::hyper::{Request as HttpRequest, Body};
// use net::build::{RequestBuilder, BuildError};
// use net::{Method, Request, RequestBody};
// use app::Client;


// impl<R> RequestBuilder<HttpRequest<()>> for R
//    where R: Request + RequestBody<()>
// {
// 	fn build(&self, client: &Client) -> Result<HttpRequest<()>, BuildError> {
// 		let mut req = HttpRequest::new(Default::default());
// 		*req.method_mut() = Self::METHOD;
// 		*req.uri_mut() = self.uri(&client).parse()?;
// 		self.headers_to(&client, req.headers_mut());
// 		Ok(req)
// 	}
// }

// impl<R, B> RequestBuilder<HttpRequest<String>> for R
// 	where R: Request<Body = B> + RequestBody<B>,
// 	      B: ::serde::Serialize + Clone
// {
// 	fn build(&self, client: &Client) -> Result<HttpRequest<String>, BuildError> {
// 		let body = match self.body(&client) {
// 			Some(body) => {
// 				debug_assert!(Self::METHOD != Method::GET);
// 				::serde_json::ser::to_string(&body)?
// 			},
// 			None => Default::default(),
// 		};

// 		let mut req = HttpRequest::new(body);
// 		*req.method_mut() = Self::METHOD;
// 		*req.uri_mut() = self.uri(&client).parse()?;
// 		self.headers_to(&client, req.headers_mut());
// 		Ok(req)
// 	}
// }

// impl<R, B> RequestBuilder<HttpRequest<Body>> for R
// 	where R: Request<Body = B> + RequestBody<B>,
// 	      B: ::serde::Serialize + Clone
// {
// 	fn build(&self, client: &Client) -> Result<HttpRequest<Body>, BuildError> {
// 		let body = match self.body(&client) {
// 			Some(body) => {
// 				debug_assert!(Self::METHOD != Method::GET);
// 				let s = ::serde_json::ser::to_string(&body)?;
// 				Body::from(s)
// 			},
// 			None => Default::default(),
// 		};

// 		let mut req = HttpRequest::new(body);
// 		*req.method_mut() = Self::METHOD;
// 		*req.uri_mut() = self.uri(&client).parse()?;
// 		self.headers_to(&client, req.headers_mut());
// 		Ok(req)
// 	}
// }
