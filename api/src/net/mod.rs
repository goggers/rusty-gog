// #[cfg(feature = "use-hyper")]
// #[cfg(feature = "use-reqwest")]
// compile_error!("U cannot use both features `reqwest` & `hyper` at one time\
//                 because the `reqwest` is already built on `hyper`.");

use std::io::Read;
use std::borrow::Cow;
use serde_json;
use serde::ser::Serialize;
use serde::de::DeserializeOwned;
use traits::*;
use error::*;


pub mod build;
pub mod headers;
use self::headers::*;


#[cfg(feature = "http")]
// #[cfg(not(feature = "hyper"))]
pub mod http;
#[cfg(feature = "http")]
use http::header::{HeaderMap, HeaderValue};

#[cfg(feature = "hyper")]
pub mod hyper;

// #[cfg(feature = "reqwest")]
// pub mod reqwest;

// #[cfg(feature = "futures")]
// pub mod futures;


pub const PROTOCOL: &str = "https";
pub type Result<T> = ::std::result::Result<T, ApiError>;


#[cfg(feature = "http")]
pub type Method = ::http::Method;
#[cfg(not(feature = "http"))]
mod method;
#[cfg(not(feature = "http"))]
pub use self::method::Method;


pub trait Request {
	const AUTH: bool = true;
	const METHOD: Method = Method::GET;

	fn uri<'a, C: IClient>(&self, client: &C) -> Cow<'a, str>;

	#[cfg(feature = "http")]
	fn headers_to<Client: IClient>(&self, client: &Client, headers: &mut HeaderMap<HeaderValue>) {
		use http::header::AUTHORIZATION;
		if Self::AUTH {
			if let Some(token) = &client.auth_token() {
				headers.insert(AUTHORIZATION, (&token.access_token).into());
			}
		}
	}

	#[inline]
	#[cfg(feature = "http")]
	fn headers<Client: IClient>(&self, client: &Client) -> HeaderMap<HeaderValue> {
		let mut map = Default::default();
		self.headers_to(client, &mut map);
		return map;
	}


	// for RequestBody default impls //
	type Body: Serialize;

	// for ResponseDeserializer default impls //
	type Response: DeserializeOwned;
}

pub trait RequestBody<Body: Serialize + Clone>
	where Self: Request<Body = Body> {
	fn body<Client: IClient>(&self, client: &Client) -> Option<Body>;
}

pub trait RequestBodyRef<'a, Body: Serialize>
	where Self: Request<Body = Body> {
	fn body_ref<Client: IClient>(&self, client: &Client) -> Option<&'a Body>;
}

pub trait ResponseDeserializer<Response: DeserializeOwned> {
	fn deserialize_response<R: Read>(mut response: R) -> Result<Response> {
		let mut src = String::new();
		response.read_to_string(&mut src)?; // io:Error into ApiError

		match serde_json::from_str::<Response>(&src) {
			Ok(result) => Ok(result),
			Err(error) => {
				// if let Ok(gog_error) = serde_json.from_reader(response)
				if let Ok(gog_error) = serde_json::from_str(&src) {
					Err(ApiError::Gog(gog_error))
				} else {
					Err(error.into())
				}
			},
		}
	}

	fn response<R: Read>(&self, response: R) -> Result<Response> { Self::deserialize_response::<R>(response) }
}


// Default impls //

impl<T> RequestBody<()> for T where T: Request<Body = ()> {
	fn body<Client: IClient>(&self, _client: &Client) -> Option<()> { None }
}

impl<T> ResponseDeserializer<()> for T where T: Request<Response = ()> {
	fn deserialize_response<R: Read>(mut _response: R) -> ::net::Result<()> { Ok(()) }
}
