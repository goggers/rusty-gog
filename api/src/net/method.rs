use self::Inner::*;

#[derive(PartialEq, Eq)]
enum Inner {
	Options,
	Get,
	Post,
	Put,
	Delete,
	Head,
	Trace,
	Connect,
	Patch,
}

#[derive(PartialEq, Eq)]
pub struct Method(Inner);

impl Method {
	pub const GET: Method = Method(Get);
	pub const POST: Method = Method(Post);
	pub const PUT: Method = Method(Put);
	pub const DELETE: Method = Method(Delete);
	pub const HEAD: Method = Method(Head);
	pub const OPTIONS: Method = Method(Options);
	pub const CONNECT: Method = Method(Connect);
	pub const PATCH: Method = Method(Patch);
	pub const TRACE: Method = Method(Trace);
}
