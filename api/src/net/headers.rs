#[cfg(feature = "http")]
pub use http::header::AUTHORIZATION;
#[cfg(feature = "http")]
pub use http::header::{HeaderMap, HeaderValue};


/// cbindgen:field-names=[value]
#[cfg_attr(feature = "cffi", repr(C))]
#[cfg_attr(feature = "cffi", no_mangle)]
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Bearer(String);

static PREFIX: &'static str = "Bearer ";

impl Bearer {
	pub fn new(token: &str) -> Self {
		let mut value = PREFIX.to_string();
		value.push_str(token);
		Bearer(value.into())
	}

	pub fn value(&self) -> &str { &self.0 }
	pub fn token(&self) -> &str { &self.0[PREFIX.len()..] }
}


#[cfg(feature = "http")]
impl Into<HeaderValue> for Bearer {
	#[inline]
	fn into(self) -> HeaderValue { (&self).into() }
}

#[cfg(feature = "http")]
impl<'a> Into<HeaderValue> for &'a Bearer {
	#[inline]
	fn into(self) -> HeaderValue { HeaderValue::from_str(&self.value()).unwrap() }
}

#[cfg(feature = "http")]
impl Bearer {
	#[inline]
	pub fn into_cloned(&self) -> HeaderValue { self.into() }
}
