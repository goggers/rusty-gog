use cfg::ds::Config;
use app::TokenState;
use app::Application;
use app::Descriptor;
use auth::ds::AuthToken;
use social::ds::SocialToken;


pub trait IClient {
	fn application(&self) -> &Application;
	fn descriptor(&self) -> &Descriptor;
	fn config(&self) -> Option<&Config>;
	fn auth_token(&self) -> Option<&AuthToken>;
	fn social_token(&self) -> Option<&SocialToken>;

	fn auth_token_state(&self) -> TokenState;
	fn social_token_state(&self) -> TokenState;
}

pub trait IClientMut: IClient {
	fn config_mut(&mut self) -> &mut Option<Config>;
	fn auth_token_mut(&mut self) -> &mut Option<AuthToken>;
	fn social_token_mut(&mut self) -> &mut Option<SocialToken>;
}


// parts: //

pub trait IClientInfo {
	fn application(&self) -> &Application;
	fn descriptor(&self) -> &Descriptor;
}

pub trait IClientAuth {
	fn auth_token(&self) -> Option<&AuthToken>;
	fn social_token(&self) -> Option<&SocialToken>;

	fn auth_token_state(&self) -> TokenState;
	fn social_token_state(&self) -> TokenState;
}

pub trait IClientAuthMut {
	fn auth_token_mut(&mut self) -> &mut Option<AuthToken>;
	fn social_token_mut(&mut self) -> &mut Option<SocialToken>;
}


impl<'a, 'b, 'c, A, I> IClient for (&'a A, &'b I, &'c Config)
	where A: IClientAuth,
	      I: IClientInfo
{
	fn application(&self) -> &Application { self.1.application() }
	fn descriptor(&self) -> &Descriptor { self.1.descriptor() }
	fn config(&self) -> Option<&Config> { Some(&self.2) }

	fn auth_token(&self) -> Option<&AuthToken> { self.0.auth_token() }
	fn social_token(&self) -> Option<&SocialToken> { self.0.social_token() }

	fn auth_token_state(&self) -> TokenState { self.0.auth_token_state() }
	fn social_token_state(&self) -> TokenState { self.0.social_token_state() }
}


impl<'a, 'b, 'c, A, I> IClient for (&'a A, &'b I, Option<&'c Config>)
	where A: IClientAuth,
	      I: IClientInfo
{
	fn application(&self) -> &Application { self.1.application() }
	fn descriptor(&self) -> &Descriptor { self.1.descriptor() }
	fn config(&self) -> Option<&Config> { self.2.clone() }

	fn auth_token(&self) -> Option<&AuthToken> { self.0.auth_token() }
	fn social_token(&self) -> Option<&SocialToken> { self.0.social_token() }

	fn auth_token_state(&self) -> TokenState { self.0.auth_token_state() }
	fn social_token_state(&self) -> TokenState { self.0.social_token_state() }
}


impl<'a, 'b, 'c, 'o, A, I> IClient for (&'a A, &'b I, &'o Option<&'c Config>)
	where A: IClientAuth,
	      I: IClientInfo
{
	fn application(&self) -> &Application { self.1.application() }
	fn descriptor(&self) -> &Descriptor { self.1.descriptor() }
	fn config(&self) -> Option<&Config> { self.2.clone() }

	fn auth_token(&self) -> Option<&AuthToken> { self.0.auth_token() }
	fn social_token(&self) -> Option<&SocialToken> { self.0.social_token() }

	fn auth_token_state(&self) -> TokenState { self.0.auth_token_state() }
	fn social_token_state(&self) -> TokenState { self.0.social_token_state() }
}
