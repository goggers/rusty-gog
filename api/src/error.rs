use std::fmt;
use std::io::Error as IoError;
use std::error::Error as StdError;
use serde_json::Error as SerdeError;
use net::build::BuildError;


#[cfg_attr(feature = "cffi", repr(C))]
#[derive(Serialize, Deserialize, Debug)]
pub struct GogError {
	error: String,
	error_description: String,
}


#[derive(Debug)]
pub enum ApiError {
	Gog(GogError),
	Serde(SerdeError),
	Io(IoError),

	// #[cfg(feature = "http")]
	// Http(::http::Error),
	//
	#[cfg(feature = "hyper")]
	Net(::hyper::Error),

	#[cfg(feature = "reqwest")]
	Net(::reqwest::Error),
}


impl From<BuildError> for ApiError {
	fn from(error: BuildError) -> Self { error.into() }
}

impl From<SerdeError> for ApiError {
	fn from(error: SerdeError) -> Self { ApiError::Serde(error) }
}

impl From<GogError> for ApiError {
	fn from(error: GogError) -> Self { ApiError::Gog(error) }
}

impl From<IoError> for ApiError {
	fn from(error: IoError) -> Self { ApiError::Io(error) }
}

#[cfg(feature = "hyper")]
impl From<::hyper::Error> for ApiError {
	fn from(error: ::hyper::Error) -> Self { ApiError::Net(error) }
}


impl StdError for GogError {
	fn cause(&self) -> Option<&StdError> { None }
}

impl fmt::Display for GogError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "[GOG answered with error: \"{}: {}\"]", self.error, self.error_description)
	}
}


impl StdError for ApiError {
	fn cause(&self) -> Option<&StdError> {
		use self::ApiError::*;
		match self {
			Gog(error) => Some(error),
			Serde(error) => Some(error),
			Io(error) => Some(error),
			Net(error) => Some(error),
		}
	}
}

impl fmt::Display for ApiError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		use self::ApiError::*;
		match self {
			Gog(error) => write!(f, "{}", error),
			Serde(error) => write!(f, "[GOG Parse Error: {}]", error),
			Io(error) => write!(f, "[GOG IO Error: {}]", error),
			Net(error) => write!(f, "[GOG Network Error: {}]", error),
		}
	}
}
