// https://embed.gog.com/account/getFilteredProducts?mediaType=1&search=Shadowrun
// https://embed.gog.com/account/wishlist/search?hiddenFlag=0&mediaType=0&page=1&search=Shadowrun&sortBy=date_added&totalPages=2
// TODO: /public_wishlist/(int: user_id)/search
// see: https://gogapidocs.readthedocs.io/en/latest/listing.html#get--public_wishlist-(int-user_id)-search

use std::fmt;
use std::borrow::Cow;


// XXX: TEMP
// struct TagId {}
pub type TagId = u32;

pub type LibraryQueryFilteredProducts<Q, Systems, Genres, Features, Tags> =
	QueryFilteredProducts<Q, StrictMediaType, Systems, Genres, Features, Tags>;

pub type WishlistQueryFilteredProducts<Q, Systems, Genres, Features, Tags> =
	QueryFilteredProducts<Q, MediaType, Systems, Genres, Features, Tags>;


pub trait AsQuery {
	fn as_query<'a>(&self) -> Cow<'a, str>;
}


pub struct QueryFilteredProducts<Q, MediaType, Systems, Genres, Features, Tags>
	where Q: AsRef<str>,
	      Tags: AsRef<[TagId]>,
	      Genres: AsRef<[Genre]>,
	      Features: AsRef<[Feature]>,
	      Systems: AsRef<[System]> {
	pub media_type: MediaType,

	/// Have highest priority.
	/// So if `Some` then some filters can be omited by server.
	pub search: Option<Q>,

	pub sort_by: Option<ProductsSort>,

	/// filter only this OSs
	pub systems: Option<Systems>,
	/// filter only this features
	pub features: Option<Features>,
	/// filter only this genres
	pub genres: Option<Genres>,
	/// filter only this languages
	pub languages: Option<()>, // ? &str
	/// filter only this tags
	/// (for account only)
	pub tags: Option<Tags>,
	/// (for account only)
	pub hidden_flag: Option<bool>, // 0/1 "show hidden only", def=0
	// (for account::wishlist only)
	pub price: Option<Price>,
}

impl<Q, MT, S, G, F, Tg> AsQuery for QueryFilteredProducts<Q, MT, S, G, F, Tg>
	where MT: AsQuery,
	      Q: AsRef<str>,
	      Tg: AsRef<[TagId]>,
	      G: AsRef<[Genre]>,
	      F: AsRef<[Feature]>,
	      S: AsRef<[System]>
{
	/// e.g.: "mediaType=1&sortBy=title&price=6&search=Search
	/// &hiddenFlag=1=1=1&tags=442682905,42&system=1,32&feature=256,1024&category=3,87"
	fn as_query<'a>(&self) -> Cow<'a, str> {
		fn fmt_slice<T: fmt::Display>(pre: &str, slice: &[T]) -> Option<String> {
			let result = {
				if let Some(first) = slice.first() {
					Some(format!(
						"{}{}",
						first,
						slice[1..].iter().map(|i| format!(",{}", i)).collect::<String>()
					))
				} else {
					None
				}
			};
			if let Some(result) = result {
				Some(format!("{}={}", pre, result))
			} else {
				None
			}
		}

		fn fmt_opt_slice<L: AsRef<[T]>, T: fmt::Display>(pre: &str, slice: &Option<L>, to: &mut Vec<String>) {
			if let Some(tags) = slice {
				to.push(fmt_slice(pre, tags.as_ref()).unwrap_or_default().into());
			}
		}

		let mut result = vec![self.media_type.as_query().into()];
		// category=3,57,87&feature=256,1024&hiddenFlag=0&language=de&mediaType=1&page=1&search=demo&sortBy=release_date_desc&system=1,2,4,8,4096&tags=442682905&totalPages=3

		if let Some(sort_by) = &self.sort_by {
			result.push(sort_by.as_query().into());
		}
		if let Some(price) = &self.price {
			result.push(price.as_query().into());
		}
		if let Some(search) = &self.search {
			result.push(format!("search={}", search.as_ref()));
		}
		if let Some(hidden_flag) = &self.hidden_flag {
			result.push(format!("hiddenFlag={:b}", *hidden_flag as u8));
		}
		if let Some(tags) = &self.tags {
			result.push(fmt_slice("tags", tags.as_ref()).unwrap_or_default().into());
		}

		fmt_opt_slice("system", &self.systems, &mut result);
		fmt_opt_slice("feature", &self.features, &mut result);
		fmt_opt_slice("category", &self.genres, &mut result);
		// fmt_opt_slice(&self.languages, &mut result);

		result.join("&").into()
		// temp plug
		// format!("sortBy={self}", self = "self").into()
	}
}

impl<Q, MT, S, G, F, Tg> Default for QueryFilteredProducts<Q, MT, S, G, F, Tg>
	where MT: AsQuery + From<u8>,
	      Q: AsRef<str>,
	      Tg: AsRef<[TagId]>,
	      G: AsRef<[Genre]>,
	      F: AsRef<[Feature]>,
	      S: AsRef<[System]>
{
	fn default() -> Self {
		use std::mem::transmute;
		QueryFilteredProducts { media_type: MT::from(MediaType::Game as u8),
		                        search: None,
		                        hidden_flag: Some(false),
		                        price: None,
		                        sort_by: None,
		                        systems: None,
		                        features: None,
		                        genres: None,
		                        languages: None,
		                        tags: None, }
	}
}


pub enum ProductsSort {
	Title,         // title
	DatePurchased, // date_purchased
	TagsOrder,     // tags
	OldestFirst,   // release_date_asc
	NewestFirst,   // release_date_desc
}
impl fmt::Display for ProductsSort {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		use self::ProductsSort::*;
		write!(f, "{}", match self {
			Title => "title",
			DatePurchased => "date_purchased",
			TagsOrder => "tags",
			OldestFirst => "release_date_asc",
			NewestFirst => "release_date_desc",
		})
	}
}
impl AsQuery for ProductsSort {
	fn as_query<'a>(&self) -> Cow<'a, str> { format!("sortBy={self}", self = self).into() }
}


#[repr(u8)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum MediaType {
	Any = 0_u8,
	Game = 1_u8,
	Movie = 2,
}
impl fmt::Display for MediaType {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{self}", self = *self as u8) }
}
impl AsQuery for MediaType {
	fn as_query<'a>(&self) -> Cow<'a, str> { format!("mediaType={self}", self = self).into() }
}
impl From<u8> for MediaType {
	fn from(i: u8) -> Self {
		match i {
			0..=2 => unsafe { ::std::mem::transmute::<u8, Self>(i) },
			_ => panic!("invalid enum index {} for MediaType", i),
		}
	}
}


#[repr(u8)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum StrictMediaType {
	Game = 1_u8,
	Movie = 2,
}
impl fmt::Display for StrictMediaType {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{self}", self = *self as u8) }
}
impl AsQuery for StrictMediaType {
	fn as_query<'a>(&self) -> Cow<'a, str> { format!("mediaType={self}", self = self).into() }
}
impl From<u8> for StrictMediaType {
	fn from(i: u8) -> Self {
		match i {
			1..=2 => unsafe { ::std::mem::transmute::<u8, Self>(i) },
			_ => panic!("invalid enum index {} for StrictMediaType", i),
		}
	}
}


#[repr(u32)]
#[derive(Debug, Copy, Clone)]
pub enum System {
	// Any = 0, // ?
	WinXP = 1,
	WinVista = 2,
	Win7 = 4,
	Win8 = 8,
	Win10 = 4096,
	/// OSX 10.6+
	MacOsX6 = 16,
	/// OSX 10.7+
	MacOsX7 = 32,
	/// Ubuntu 14.04
	Ubuntu14 = 1024,
	/// Ubuntu 16.04
	Ubuntu16 = 2048,
	/// Ubuntu 18.04
	Ubuntu18 = 8192,
}
impl fmt::Display for System {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{self}", self = *self as u32) }
}
// impl<'a> fmt::Display for [System] {
// 	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{self}", self = self.join(",")) }
// }
// TODO: impl next
// impl<T: AsRef<[System]>> AsQuery for T {
// 	fn as_query<'a>(&self) -> Cow<'a, str> { format!("system={self}", self = self.join(",")) }
// }


#[repr(u32)]
#[derive(Debug, Copy, Clone)]
// ?category=,,,
pub enum Genre {
	RolePlaying = 57,
	Indie = 3,
	Adventure = 87,
	Strategy = 56,
	Action = 60,
	Racing = 59,
	Simulation = 61,
	Shooter = 58,
	// TODO: continue list
}
impl fmt::Display for Genre {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{self}", self = *self as u32) }
}


#[repr(u32)]
#[derive(Debug, Copy, Clone)]
// ?feature=,,,
pub enum Feature {
	SinglePlayer = 1,
	MultiPlayer = 2,
	CoOp = 4,
	Achievements = 8,
	Leaderboards = 16,
	ControllerSupport = 32,
	// #[depricated("used in legacy api")]
	Multiplayer = 128,
	InDevelopment = 256,
	CloudSaves = 512,
	Overlay = 1024,
}
impl fmt::Display for Feature {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{self}", self = *self as u32) }
}

#[repr(u8)]
#[derive(Debug, Copy, Clone)]
pub enum Price {
	/// < 1 Eur
	Cheap = 1_u8,
	/// < 700 Rur
	Price2 = 2,
	/// < 1000 Rur
	Price3 = 3,
	/// < 1700 Rur
	Price4 = 4,
	/// > 1700 Rur
	Price5 = 5,
	/// in Sale
	Discounted = 6,
}
impl fmt::Display for Price {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result { write!(f, "{self}", self = *self as u8) }
}
impl AsQuery for Price {
	fn as_query<'a>(&self) -> Cow<'a, str> { format!("price={self}", self = self).into() }
}


// pagination //

pub struct PaginationQuery {
	/// starts from `1`
	pub page: u32,
	pub total_pages: Option<u32>, // totalPages
}
impl AsQuery for PaginationQuery {
	fn as_query<'a>(&self) -> Cow<'a, str> {
		let mut result = Vec::with_capacity(2);
		result.push(format!("page={}", self.page));
		if let Some(total_pages) = &self.total_pages {
			result.push(format!("totalPages={}", total_pages));
		}
		result.join("&").into()
	}
}


#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct PaginationResponsePart {
	pub sort_by: String, // TODO: enum e.g.: "date_purchased"
	pub page: u32,
	pub total_pages: u32,
	pub products_per_page: u32,

	pub total_products: u32,
	pub movies_count: u32,
	// pub tags: Vec<::ds::tags::UserTag>,
	// pub products: Vec<::serde_json::Value>,
	pub updated_products_count: u32,
	pub hidden_updated_products_count: u32,
	pub applied_filters: ::serde_json::Value,
	pub has_hidden_products: bool,

	pub content_system_compatibility: Option<::serde_json::Value>,
}


#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn fmt_feature() {
		assert_eq!("1024", format!("{}", Feature::Overlay));
	}

	#[test]
	fn fmt_system() {
		fn test(expected: &str, slice: &[System]) {
			let result = {
				if let Some(first) = slice.first() {
					format!(
					        "{}{}",
					        first,
					        slice[1..].iter().map(|i| format!(",{}", i)).collect::<String>()
					)
				} else {
					Default::default()
				}
			};
			assert_eq!(expected, format!("{}", result));
		}

		let vec = vec![System::WinXP, System::MacOsX7];
		test("1,32", &vec[..]);
		test("1", &vec[..1]);
	}

	#[test]
	fn fmt_get_filtered_products() {
		let tags = [442682905, 42];
		let features = [Feature::InDevelopment, Feature::Overlay];
		let genres = [Genre::Indie, Genre::Adventure];
		let q = GetFilteredProducts { media_type: MediaType::Game,
		                              search: Some("Search"),
		                              hidden_flag: Some(true),
		                              price: Some(Price::Discounted),
		                              sort_by: Some(ProductsSort::Title),
		                              systems: Some(vec![System::WinXP, System::MacOsX7]),
		                              features: Some(features),
		                              genres: Some(genres),
		                              languages: None,
		                              tags: Some(tags), };
		let s = q.as_query().to_owned();
		assert!(s.contains("mediaType=1"));
		assert!(s.contains("sortBy=title"));
		assert!(s.contains("search=Search"));
		assert!(s.contains("hiddenFlag=1"));
		assert!(s.contains("price=6"));
		assert!(s.contains("system=1,32"));
		assert!(s.contains("feature=256,1024"));
		assert!(s.contains("category=3,87"));
		assert!(s.contains("tags=442682905,42"));
		assert!(!s.contains("languages"));
	}

	#[test]
	fn fmt_pagination_query() {
		let q = PaginationQuery { page: 42,
		                          total_pages: Some(100), };
		let s = q.as_query().to_owned();
		assert!(s.contains("page=42"));
		assert!(s.contains("totalPages=100"));
	}


	#[test]
	fn u8_to_media_type() {
		assert_eq!(MediaType::Any, 0_u8.into());
		assert_eq!(MediaType::Game, 1_u8.into());
		assert_eq!(MediaType::Movie, 2_u8.into());
	}

	#[test]
	fn u8_to_media_type_strict() {
		assert_eq!(StrictMediaType::Game, 1_u8.into());
		assert_eq!(StrictMediaType::Movie, 2_u8.into());
	}

	#[test]
	#[should_panic(expected = "invalid enum index 3 for MediaType")]
	fn u8_to_media_type_panic_3() { let foo: MediaType = 3_u8.into(); }

	#[test]
	#[should_panic(expected = "invalid enum index 0 for StrictMediaType")]
	fn u8_to_media_type_strict_panic_0() { let foo: StrictMediaType = 0_u8.into(); }

	#[test]
	#[should_panic(expected = "invalid enum index 3 for StrictMediaType")]
	fn u8_to_media_type_strict_panic_3() { let foo: StrictMediaType = 3_u8.into(); }
}
