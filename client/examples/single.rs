#[macro_use]
extern crate lazy_static;
extern crate mut_static;
extern crate url;
extern crate futures;
use futures::*;

pub extern crate gog_api;
use gog_api as gog;
use gog::app::Client as GogClient;

extern crate gog_client;
use gog_client::*;
use error::AppError;


fn main() -> Result<(), AppError> {
	use std::time::Duration;

	let ok = |ok| println!("main ok: {:?}", ok);
	let err = |err: AppError| {
		println!("main err: {:?}", err);
		let res = init(None, false, |res| println!("init:: second try result async: {:?}", res));
		println!("init:: second try result sync: {:?}", res);
	};


	init(None, true, move |res| {
		match res {
			Result::Ok(_) => ok((true, 42)),
			Result::Err(v) => err(v),
		}
	})?;


	// sim:
	let mut i = 0;
	loop {
		println!("...");
		std::thread::sleep(Duration::from_millis(500));
		if i > 20 {
			break;
		}
		i += 1;
	}
	println!("FIN");
	Ok(())
}


pub fn init<FnComplete>(state: Option<GogClient>, online: bool, complete: FnComplete) -> Result<(), AppError>
	where FnComplete: 'static + Send + FnOnce(Result<(), AppError>) -> () {
	if let Some(state) = state {
		state::init_hot(state)?;
	} else {
		state::init_cold()?;
	}

	if online {
		println!("MODE: online");
		init_client_online(complete)?;
	} else {
		// TODO: imple "offline mode" by cache
		println!("MODE: offline");
	}
	Ok(())
}


fn init_client_online<FnComplete>(complete: FnComplete) -> Result<(), AppError>
	where FnComplete: 'static + Send + FnOnce(Result<(), AppError>) -> () {
	use fut::future_then_else;
	use gog::app::TokenState::*;
	use future::Either;

	// upd cfg -> lazy upd auth token -> lazy upd social token:
	let upd_token = upd_config(&*state::gog()?)?.and_then(move |_| {
		                                            // auth token:
		                                            let client = state::gog()?;
		                                            let next = match client.auth_token_state() {
			                                            Expired => {
			                                               println!("main token is expired!");
			                                               Either::A(upd_auth_token(&client)?)
			                                              },
		                                               _ => Either::B(future::ok(())),
		                                            };
		                                            Ok(next)
		                                           })
	                                            .flatten()
	                                            .and_then(move |_| {
		                                            // social token:
		                                            let client = state::gog()?;
		                                            let next = match (client.auth_token_state(), client.social_token_state()) {
			                                            // we can upd social token only when auth token is ok
		                                               (Normal, Expired) | (Normal, Missed) => {
			                                               println!("social token is expired!");
			                                               Either::A(upd_social_token(&client)?)
			                                              },
		                                               _ => Either::B(future::ok(())),
		                                            };
		                                            Ok(next)
		                                           })
	                                            .flatten();
	future_then_else(upd_token, complete);
	Ok(())
}


pub fn req_token_with_code<T: AsRef<str>, FnComplete>(code: T, complete: FnComplete) -> Result<(), AppError>
	where FnComplete: 'static + Send + FnOnce(Result<(), AppError>) -> () {
	use fut::future_then_else;
	let req_token = {
		                let net = state::net()?;
		                let client = &*net;
		                // TODO: opt `.as_ref().to_string()`
		                let req = gog::auth::RequestTokenByCode(code.as_ref().to_string());
		                let f = net::request(req, &client, &*state::gog()?)?;
		                f.map_err(|err| AppError::from(err))
		               }.and_then(move |token| {
		                println!("NEW AUTH TOKEN: {:#?}", token);
		                Ok(*state::gog_mut()?.auth_token_mut() = Some(token))
		               });
	future_then_else(req_token, complete);
	Ok(())
}

pub fn req_token_with_code_in_url<T: AsRef<str>, FnComplete>(url: T, complete: FnComplete) -> Result<(), AppError>
	where FnComplete: 'static + Send + FnOnce(Result<(), AppError>) -> () {
	use std::str::FromStr;
	use url::Url;

	let u = Url::from_str(url.as_ref())?;
	let mut code = None;
	for (key, value) in u.query_pairs() {
		if key == "code" {
			println!("PARSED CODE: {:?}", value);
			code = Some(value);
		}
	}
	return if let Some(code) = code {
		req_token_with_code(code, complete)
	} else {
		Err(AppError::Parse(url::ParseError::EmptyHost))
	};
}


fn get_config(gog_client: &GogClient) -> Result<impl Future<Item = gog::cfg::ds::Config, Error = AppError>, AppError> {
	let net = state::net()?;
	let client = &*net;
	let req = gog::cfg::GetConfig();
	let f = net::request(req, &client, &gog_client)?; //TODO: .map_err(|err| from_cache());
	Ok(f.map_err(|err| AppError::from(err)))
}


fn upd_config(gog_client: &GogClient) -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	Ok(get_config(&gog_client)?.and_then(move |cfg| Ok(*state::gog_mut()?.config_mut() = Some(cfg))))
}


// TODO: mb. move to the C-Wrapper
pub fn client_need_login() -> Result<bool, AppError> { Ok(state::is_login_needed(&*state::gog()?)) }

pub fn get_login_uri() -> Result<String, AppError> {
	let state = state::gog()?;
	Ok(gog::auth::get_auth_uri(&*state))
}


fn upd_auth_token(gog_client: &GogClient) -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	let net = state::net()?;
	let client = &*net;
	let req = gog::auth::RefreshToken();
	Ok(net::request(req, &client, &gog_client)?.map_err(|err| AppError::from(err))
	                                           .and_then(move |token| {
		                                           println!("NEW AUTH TOKEN: {:#?}", token);
		                                           Ok(*state::gog_mut()?.auth_token_mut() = Some(token))
		                                          }))
}


fn upd_social_token(gog_client: &GogClient) -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	let net = state::net()?;
	let client = &*net;
	let req = gog::social::MenuAuth();
	Ok(net::request(req, &client, &gog_client)?.map_err(|err| AppError::from(err))
	                                           .and_then(move |resp| {
		                                           println!("NEW SOCIAL TOKEN: {:#?}", resp);
		                                           Ok(*state::gog_mut()?.social_token_mut() = Some(resp.token))
		                                          }))
}


pub mod state {
	use super::*;
	use net::{self, HyperClient};
	use mut_static::MutStatic;
	use mut_static::Error as MutStaticError;
	use mut_static::ForceSomeRwLockReadGuard;
	use mut_static::ForceSomeRwLockWriteGuard;


	pub type CID = u8;


	lazy_static! {
		static ref GOG: MutStatic<GogClient> = { MutStatic::new() };
		static ref NET: MutStatic<HyperClient> = { MutStatic::new() };
	}


	#[inline]
	pub fn gog<'a>() -> Result<ForceSomeRwLockReadGuard<'a, GogClient>, MutStaticError> { GOG.read() }

	#[inline]
	pub fn gog_mut<'a>() -> Result<ForceSomeRwLockWriteGuard<'a, GogClient>, MutStaticError> { GOG.write() }

	#[inline]
	pub fn net<'a>() -> Result<ForceSomeRwLockReadGuard<'a, HyperClient>, MutStaticError> { NET.read() }


	pub fn init_net() -> Result<(), MutStaticError> {
		if !NET.is_set()? {
			NET.set(net::create_client().unwrap())?;
		}
		Ok(())
	}

	/// cold start
	pub fn init_cold() -> Result<(), MutStaticError> {
		init_hot(GogClient::default())?;
		Ok(())
	}


	/// hot start with prepared state from prev. session
	pub fn init_hot(client: GogClient) -> Result<(), MutStaticError> {
		init_net()?;

		if !GOG.is_set()? {
			GOG.set(client)?;
		} else {
			let old = ::std::mem::replace(&mut *GOG.write()?, client);
			println!("re-init: gog-client replaced: {:#?}", old);
		}
		Ok(())
	}


	pub fn is_login_needed(client: &GogClient) -> bool {
		use gog::app::TokenState::*;
		match client.auth_token_state() {
			Missed => true,
			_ => false,
		}
	}
}
