#[macro_use]
extern crate lazy_static;
extern crate mut_static;
extern crate gog_client;
use gog_client::*;
use state::Ctx;
use error::AppError;


fn main() -> Result<(), AppError> {
	use std::time::Duration;

	lazy_static! {
		static ref CTX: Vec<Ctx> = {
			(0..state::CTX_MAX).into_iter()
			                   .map(|_| state::create_context().unwrap())
			                   .collect::<Vec<_>>()
		};
	}

	println!("spawning {} clients in parallel", state::CTX_MAX);
	for i in 0..state::CTX_MAX {
		let ctx = CTX.get(i).unwrap();

		std::thread::spawn(move || {
			let i = i.clone();
			let ok = move |ok| println!("[async {}] call init result: OK: {:?}", &i, ok);
			let err = move |err: AppError| {
				println!("[async {}] call init result: ERR: {:?}", &i, err);
				// TODO: reinit in offline mode:
				// let res = init(&ctx, None, false, |res| println!("init:: second try result async: {:?}", res));
				// println!("init:: second try result sync: {:?}", res);
			};

			let res = init(&*ctx, None, true, move |res| {
				match res {
					Result::Ok(_) => ok((true, 42)),
					Result::Err(v) => err(v),
				}
			});
			println!("[sync {}] call init result: {:?}", i, res);
		});
		// std::thread::sleep(Duration::from_millis(0));
	}


	// sim:
	let mut i = 0;
	loop {
		println!("...");
		std::thread::sleep(Duration::from_millis(500));
		if i > 20 {
			break;
		}
		i += 1;
	}
	println!("FIN");
	Ok(())
}
