use hyper;
use hyper::client::HttpConnector;
use hyper_tls::{Error as TlsError, HttpsConnector};
use serde::de::DeserializeOwned;
use futures::{future, Future, Stream};

use gog::error::ApiError as GogApiError;
use gog::net::build::{BuildError, RequestBuilder};
use gog::net::Request as GogRequest;
use gog::cfg::ds::Config;
use gog::traits::*;

use state;
use error::AppError;
use fut::DeserializeFutureOwned;


pub type HyperClient = hyper::Client<HttpsConnector<HttpConnector>>;
pub type HyperRequest = hyper::Request<hyper::Body>;


pub fn create_client() -> Result<HyperClient, TlsError> {
	use hyper::*;
	// 4 is number of blocking DNS threads
	let mut https = HttpsConnector::new(4)?;
	https.https_only(true);

	let client = Client::builder().build::<_, Body>(https);
	return Ok(client);
}


// TODO: impl safe_request with check token expiration


pub fn request<Req: Sized, Resp: Sized, GogClient: IClient>(
	req: Req, client: &HyperClient, gog_client: &GogClient)
	-> Result<impl Future<Item = Resp, Error = GogApiError>, BuildError>
	where Req: GogRequest<Response = Resp> + RequestBuilder<HyperRequest>,
	      Resp: DeserializeOwned + Send {
	let f = {
		let request: HyperRequest = req.build(gog_client)?;
		let got_resp = client.request(request);
		future::lazy(move || got_resp.map_err(|e| GogApiError::Net(e))).and_then(|resp| {
			resp.into_body()
			    .concat2()
			    .map_err(|e| GogApiError::Net(e))
			    .and_then(|chunk| DeserializeFutureOwned::<Resp, _>::new(chunk))
		})
	};
	Ok(future::lazy(move || f))
}


pub fn request_ext<Req, Resp, GA, GI>(req: Req, client: &HyperClient, gog_auth: &GA, gog_info: &GI, cfg: &Option<&Config>)
                                  -> Result<impl Future<Item = Resp, Error = GogApiError>, BuildError>
	where Req: Sized,
	      Resp: Sized,
	      GA: IClientAuth,
	      GI: IClientInfo,
	      Req: GogRequest<Response = Resp> + RequestBuilder<HyperRequest>,
	      Resp: DeserializeOwned + Send {
	let f = {
		use gog::net::build::RequestBuilderUsingParts;
		let request: HyperRequest = req.build_from(gog_auth, gog_info, cfg)?;
		println!("requesting:: {:#?}", request);

		let got_resp = client.request(request);
		future::lazy(move || got_resp.map_err(|e| GogApiError::Net(e))).and_then(|resp| {
			println!("Response.Status: {}", &resp.status());
			println!("Response.Headers: {:#?}", &resp.headers());

			resp.into_body()
			    .concat2()
			    .map_err(|e| GogApiError::Net(e))
			    .and_then(|chunk| DeserializeFutureOwned::<Resp, _>::new(chunk))
		})
	};
	Ok(future::lazy(move || f))
}


/// Build request `future` using client-data stored in mem as `static mut`.
pub fn req<Req, Resp>(req: Req, cid: state::CID) -> Result<impl Future<Item = Resp, Error = GogApiError>, AppError>
	where Req: Sized,
	      Resp: Sized,
	      Req: GogRequest<Response = Resp> + RequestBuilder<HyperRequest>,
	      Resp: DeserializeOwned + Send {
	let net = state::net()?;
	let auth = state::auth(cid)?;
	let info = state::info()?;
	let cfg = state::cfg()?;
	let cfg = if let Some(cfg) = &*cfg { Some(&*cfg) } else { None };
	request_ext(req, &*net, &*auth, &*info, &cfg).map_err(|err| AppError::from(err))
}
