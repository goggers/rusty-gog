use std::io::Error as IoError;
use hyper::Error as NetError;
use hyper_tls::Error as TlsError;
#[cfg(feature = "rustbreak")]
use rustbreak::BreakError;
use url::ParseError as UrlParseError;
use serde_json::Error as SerdeError;
use mut_static::Error as MutStaticError;
use gog::{error::{ApiError, GogError}, net::build::BuildError};
use std::error::Error as StdError;
use std::fmt;

// TODO: impl from std::option::NoneError

#[derive(Debug)]
pub enum AppError {
	Gog(GogError),

	Serde(SerdeError),
	Parse(UrlParseError),

	Io(IoError),
	Tls(TlsError),
	Net(NetError),
	Build(BuildError),

	MutStatic(MutStaticError),
	#[cfg(feature = "rustbreak")]
	Database(BreakError),

	InvalidCtxId(::state::CID),

	Other(String),
}

impl StdError for AppError {
	fn cause(&self) -> Option<&StdError> {
		use AppError::*;
		match self {
			Gog(error) => Some(error),
			Serde(error) => Some(error),
			Io(error) => Some(error),
			Tls(error) => Some(error),
			Net(error) => Some(error),
			Parse(error) => Some(error),
			Build(error) => Some(error),
			MutStatic(error) => Some(error),
			InvalidCtxId(_) => None,
			Other(_) => None,
		}
	}
}

impl fmt::Display for AppError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		use AppError::*;
		match self {
			Gog(error) => write!(f, "{}", error),
			Build(error) => write!(f, "{}", error),
			Serde(error) => write!(f, "[GOG Parse Error: {}]", error),
			Tls(error) => write!(f, "[GOG Network Error: {}]", error),
			Net(error) => write!(f, "[GOG Network Error: {}]", error),
			Io(error) => write!(f, "[GOG IO Error: {}]", error),
			Parse(error) => write!(f, "[GOG Parse Error: {}]", error),
			MutStatic(error) => write!(f, "[GOG Internal Error: {}]", error),
			InvalidCtxId(id) => write!(f, "[GOG Client Error: invalid context ID: {}]", id),
			Other(s) => write!(f, "[GOG Unknown Error: {}]", s),
		}
	}
}


impl From<IoError> for AppError {
	fn from(error: IoError) -> Self { AppError::Io(error) }
}

impl From<ApiError> for AppError {
	fn from(error: ApiError) -> Self {
		use AppError::*;
		match error {
			ApiError::Io(error) => Io(error),
			ApiError::Net(error) => Net(error),
			ApiError::Gog(error) => Gog(error),
			ApiError::Serde(error) => Serde(error),
		}
	}
}

impl From<BuildError> for AppError {
	fn from(error: BuildError) -> Self { AppError::Build(error) }
}

#[cfg(feature = "rustbreak")]
impl From<BreakError> for AppError {
	fn from(error: BreakError) -> Self { AppError::Database(error) }
}

impl From<TlsError> for AppError {
	fn from(error: TlsError) -> Self { AppError::Tls(error) }
}

impl From<UrlParseError> for AppError {
	fn from(error: UrlParseError) -> Self { AppError::Parse(error) }
}

impl From<MutStaticError> for AppError {
	fn from(error: MutStaticError) -> Self { AppError::MutStatic(error) }
}
