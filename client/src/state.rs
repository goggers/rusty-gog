//! Very strange & dirty implementation of multicontextual (multi-)state.
//! TODO: rewrite it at all
// use std::mem;
// use std::ops::{Deref, DerefMut};
// use std::ops::Deref;

use mut_static::MutStatic;
use mut_static::Error as MutStaticError;
use mut_static::ForceSomeRwLockReadGuard;
use mut_static::ForceSomeRwLockWriteGuard;

use gog::cfg::ds::Config;
use gog::app::Descriptor;
use gog::app::Application;
use gog::auth::ds::AuthToken;
use gog::social::ds::SocialToken;
use gog::social::ds::MenuUserInfo;
use gog::app::TokenState;
use gog::traits::*;

use net::{self, HyperClient};
use error::AppError;


macro_rules! GOGX {
	(0) => { GOG0 };
	(1) => { GOG1 };
	(2) => { GOG2 };
	(3) => { GOG3 };
}

macro_rules! GOG {
	(0) => { GOGX!(0) };
	(1) => { GOGX!(1) };
	(2) => { GOGX!(2) };
	(3) => { GOGX!(3) };
	// runtime part:
	($id:expr, r) => {
		match $id {
			0 => { GOGX!(0).read().map_err(|err| AppError::from(err)) },
			1 => { GOGX!(1).read().map_err(|err| AppError::from(err)) },
			2 => { GOGX!(2).read().map_err(|err| AppError::from(err)) },
			3 => { GOGX!(3).read().map_err(|err| AppError::from(err)) },
			_ => { Err(AppError::InvalidCtxId($id)) },
		}
	};
	($id:expr, rw) => {
		match $id {
			0 => { GOGX!(0).write().map_err(|err| AppError::from(err)) },
			1 => { GOGX!(1).write().map_err(|err| AppError::from(err)) },
			2 => { GOGX!(2).write().map_err(|err| AppError::from(err)) },
			3 => { GOGX!(3).write().map_err(|err| AppError::from(err)) },
			_ => { Err(AppError::InvalidCtxId($id)) },
		}
	};
	($id:expr, is) => {
		match $id {
			0 => { GOGX!(0).is_set().map_err(|err| AppError::from(err)) },
			1 => { GOGX!(1).is_set().map_err(|err| AppError::from(err)) },
			2 => { GOGX!(2).is_set().map_err(|err| AppError::from(err)) },
			3 => { GOGX!(3).is_set().map_err(|err| AppError::from(err)) },
			_ => { Err(AppError::InvalidCtxId($id)) },
		}
	};
	($id:expr, set, $value:expr) => {
		match $id {
			0 => { GOGX!(0).set($value).map_err(|err: MutStaticError| AppError::from(err)) },
			1 => { GOGX!(1).set($value).map_err(|err: MutStaticError| AppError::from(err)) },
			2 => { GOGX!(2).set($value).map_err(|err: MutStaticError| AppError::from(err)) },
			3 => { GOGX!(3).set($value).map_err(|err: MutStaticError| AppError::from(err)) },
			_ => { Err(AppError::InvalidCtxId($id)) },
		}
	};
}

macro_rules! GOGR { ($id:expr) => { GOG!($id, r) }; }
macro_rules! GOGRW { ($id:expr) => { GOG!($id, rw) }; }
macro_rules! GOGIS { ($id:expr) => { GOG!($id, is) }; }


lazy_static! {
	static ref GOG0: MutStatic<AuthClient> = { MutStatic::new() };
	static ref GOG1: MutStatic<AuthClient> = { MutStatic::new() };
	static ref GOG2: MutStatic<AuthClient> = { MutStatic::new() };
	static ref GOG3: MutStatic<AuthClient> = { MutStatic::new() };

	static ref CONFIG: MutStatic<Option<Config>> = { MutStatic::from(None) };
	static ref INFO: MutStatic<ClientInfo> = MutStatic::from(ClientInfo { ..Default::default() });
	static ref NET: MutStatic<HyperClient> = { MutStatic::from(net::create_client().unwrap()) };
}


pub fn info<'a>() -> Result<ForceSomeRwLockReadGuard<'a, ClientInfo>, AppError> {
	INFO.read().map_err(|err| AppError::from(err))
}

#[inline]
pub fn auth<'a>(id: CID) -> Result<ForceSomeRwLockReadGuard<'a, AuthClient>, AppError> { gog(id) }

pub fn cfg<'a>() -> Result<ForceSomeRwLockReadGuard<'a, Option<Config>>, AppError> {
	CONFIG.read().map_err(|err| AppError::from(err))
}

pub type ConfigState = TokenState;
pub fn cfg_state() -> Result<ConfigState, AppError> {
	use self::TokenState::{Expired, Missed, Normal};
	Ok({
		match &*cfg()? {
			Some(cfg) => {
				if cfg.expired() {
					Expired
				} else {
					Normal
				}
			},
			None => Missed,
		}
	})
}

pub fn cfg_mut<'a>() -> Result<ForceSomeRwLockWriteGuard<'a, Option<Config>>, MutStaticError> { CONFIG.write() }

pub fn cfg_set(cfg: Config) -> Result<(), AppError> {
	#[cfg(feature = "option_replace")]
	{ cfg_mut()?.replace(cfg); }
	#[cfg(not(feature = "option_replace"))]
	{ *cfg_mut()? = Some(cfg); }
	Ok(())
}


#[derive(Default, Debug)]
pub struct ClientInfo {
	application: Application,
	descriptor: Descriptor,
}

#[derive(Default, Debug)]
#[cfg_attr(feature = "cffi", repr(C))]
pub struct AuthClient {
	auth_token: Option<AuthToken>,
	social_token: Option<SocialToken>,
}


impl IClientInfo for ClientInfo {
	fn application(&self) -> &Application { &self.application }
	fn descriptor(&self) -> &Descriptor { &self.descriptor }
}


impl IClientAuth for AuthClient {
	fn auth_token(&self) -> Option<&AuthToken> {
		match &self.auth_token {
			Some(res) => Some(&res),
			None => None,
		}
	}

	fn social_token(&self) -> Option<&SocialToken> {
		match &self.social_token {
			Some(res) => Some(&res),
			None => None,
		}
	}

	fn auth_token_state(&self) -> TokenState { AuthToken::state(&self.auth_token()) }
	fn social_token_state(&self) -> TokenState { SocialToken::state(&self.social_token()) }
}

impl IClientAuthMut for AuthClient {
	fn auth_token_mut(&mut self) -> &mut Option<AuthToken> { &mut self.auth_token }
	fn social_token_mut(&mut self) -> &mut Option<SocialToken> { &mut self.social_token }
}





#[inline]
pub fn gog<'a>(id: CID) -> Result<ForceSomeRwLockReadGuard<'a, AuthClient>, AppError> { GOGR!(id) }

#[inline]
pub fn gog_mut<'a>(id: CID) -> Result<ForceSomeRwLockWriteGuard<'a, AuthClient>, AppError> { GOGRW!(id) }

#[inline]
pub fn net<'a>() -> Result<ForceSomeRwLockReadGuard<'a, HyperClient>, MutStaticError> { NET.read() }


/// cold start
pub fn init_cold<D>(ctx: &Ctx<'_, D>) -> Result<(), AppError> { init_hot(ctx, AuthClient::default()) }

/// hot start with prepared state from prev. session
pub fn init_hot<D>(ctx: &Ctx<'_, D>, client: AuthClient) -> Result<(), AppError> {
	if !GOG!(ctx.id(), is)? {
		GOG!(ctx.id(), set, client)
	} else {
		use std::mem::replace;
		use std::ops::DerefMut;
		println!("replacing client: new: {:?}", &client);
		replace(gog_mut(ctx.id())?.deref_mut(), client);
		Ok(())
	}
}

/// hot start with prepared state from prev. session
pub fn is_inited<'a, D, C: Context<'a, D>>(ctx: &C) -> Result<bool, AppError> { GOG!(ctx.id(), is) }


pub const CTX_MAX: usize = 4;
static mut LOCKED_CTX: [bool; CTX_MAX] = [false, false, false, false];

pub type CID = u8;

#[derive(Debug, Clone)]
#[cfg_attr(feature = "cffi", repr(C))]
pub struct Ctx<'a, D: 'a> {
	id: CID,
	delegate: Option<&'a D>,
}

impl<'a, D> Context<'a, D> for Ctx<'a, D> {
	fn id(&self) -> CID { self.id }
	fn id_ref(&self) -> &CID { &self.id }
	fn delegate(&self) -> Option<&'a D> { self.delegate.clone() }
	fn delegate_mut(&mut self) -> &mut Option<&'a D> { &mut self.delegate }
}

impl<'a, D, T: Context<'a, D>> From<&'a T> for Ctx<'a, D> {
	fn from(ctx: &T) -> Self {
		Self { id: ctx.id(),
		       delegate: ctx.delegate(), }
	}
}


pub trait Context<'a, D> {
	fn id(&self) -> CID;
	fn id_ref(&self) -> &CID;
	fn delegate(&self) -> Option<&'a D>;
	fn delegate_mut(&mut self) -> &mut Option<&'a D>;
}

pub trait Delegate: Send + Sync {
	fn initialized(&self);
	fn logged_in(&self);
	fn config_upd(&self, v: &Config);
	fn auth_token_upd<T: IClientAuth>(&self, v: &T);
	fn social_token_upd<T: IClientAuth>(&self, v: &T);
	fn social_user_info(&self, v:MenuUserInfo);
}

impl<'a, D> Ctx<'a, D> {
	pub fn new(id: CID, delegate: Option<&'a D>) -> Self { Self { id, delegate } }
	pub fn new_from(source: &Self) -> Self {
		Self { id: source.id(),
		       delegate: source.delegate(), }
	}
}


pub fn create_context<'a, D: Delegate>() -> Result<Ctx<'a, D>, AppError> {
	let id = get_free_cid()?;
	unsafe { LOCKED_CTX[id as usize] = true }
	Ok(Ctx::new(id, None))
}

fn get_free_cid() -> Result<CID, AppError> {
	for i in 0..CTX_MAX {
		let icid = i as CID;
		if !unsafe { LOCKED_CTX[i] } && !GOGIS!(icid)? {
			return Ok(icid);
		}
	}

	// TODO: Make spetial error for this case.
	Err(AppError::InvalidCtxId(42))
}


pub fn is_login_needed<T: IClientAuth>(client: &T) -> bool {
	use gog::app::TokenState::*;
	match client.auth_token_state() {
		Missed => true,
		_ => false,
	}
}
