use std;
use std::marker::PhantomData;
use futures::{self, prelude::*, Future, Stream, Async};
use serde::de::{Deserialize, DeserializeOwned};
use tokio;

use gog::net::{Request, build::{RequestBuilder, BuildError}};
use gog::error::ApiError;
use gog::traits::IClient;

use net::{HyperClient, HyperRequest};


pub struct DeserializeFutureOwned<T: DeserializeOwned, R: AsRef<[u8]>> {
	raw: R,
	__: PhantomData<T>,
}

impl<T, R> DeserializeFutureOwned<T, R>
	where T: DeserializeOwned,
	      R: AsRef<[u8]>
{
	pub fn new(raw: R) -> Self {
		Self { raw: raw,
		       __: unsafe { ::std::mem::uninitialized() }, }
	}
}

impl<T, R> Future for DeserializeFutureOwned<T, R>
	where T: DeserializeOwned,
	      R: AsRef<[u8]>
{
	type Item = T;
	type Error = ApiError;

	fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
		use serde_json;
		// same as `ResponseDeserializer::deserialize_response()`
		match serde_json::from_slice::<Self::Item>(&self.raw.as_ref()) {
			Ok(result) => Ok(Async::Ready(result)),
			Err(error) => {
				if let Ok(gog_error) = serde_json::from_slice(&self.raw.as_ref()) {
					Err(ApiError::Gog(gog_error))
				} else {
					Err(error.into())
				}
			},
		}
	}
}


pub fn to_future<T, R, Gog>(req: &T, gog_client: &Gog, net_client: &HyperClient)
                            -> ::std::result::Result<impl Future<Item = R, Error = ApiError>, BuildError>
	where R: Send,
	      Gog: IClient,
	      for<'de> R: Deserialize<'de>,
	      T: Request<Response = R> + RequestBuilder<HyperRequest> {
	Ok({
		let request: HyperRequest = req.build(gog_client)?;
		println!("requesting:: {:#?}", request);

		let map_err = ApiError::from;
		let got_resp = net_client.request(request).map_err(map_err).and_then(move |resp| {
			println!("Response.Status: {}", &resp.status());
			// println!("Response.Headers: {:#?}", &resp.headers());

			resp.into_body()
			    .concat2()
			    .map_err(map_err)
			    .and_then(|chunk| DeserializeFutureOwned::<R, _>::new(chunk))
		});
		got_resp
	})
}


pub type OkFn<T> = fn(T) -> ();
pub type ErrFn<T> = OkFn<T>;
pub type ResFn<O, E> = fn(Result<O, E>) -> ();

pub fn future_then_callback<F, OkFn, ErrFn, Ok, Err, FromErr>(f: F, ok: OkFn, err: ErrFn)
	where Ok: 'static + Send,
	      Err: 'static + Send,
	      OkFn: 'static + Send + Fn(Ok) -> (),
	      FromErr: Send + From<Err>,
	      ErrFn: 'static + Send + Fn(FromErr) -> (),
	      F: 'static + Send + Future<Item = Ok, Error = Err> {
	let mut runtime = tokio::runtime::Runtime::new().unwrap();
	let (tx, rx) = futures::sync::oneshot::channel();
	let f = f.then(move |res| tx.send(res).map_err(|_err| ()));
	runtime.spawn(f);

	let watcher = rx.then(|res| res.unwrap())
	                .then(move |res| {
		                match res {
			                Result::Ok(v) => ok(v),
		                   Result::Err(v) => err(FromErr::from(v)),
		                }
		                Ok(())
		               })
	                .join(runtime.shutdown_on_idle());
	std::thread::spawn(move || watcher.wait());

	// future_then_future(f, move |res| {
	// 	match res {
	// 		Result::Ok(v) => ok(v),
	// 		Result::Err(v) => err(v),
	// 	}
	// });
}


pub fn future_then_else<F, Ok, Err, FnComplete>(f: F, complete: FnComplete)
	where Ok: 'static + Send,
	      Err: 'static + Send,
	      F: 'static + Send + Future<Item = Ok, Error = Err>,
	      FnComplete: 'static + Send + FnOnce(Result<Ok, Err>) -> () {
	let mut runtime = tokio::runtime::Runtime::new().unwrap();
	let (tx, rx) = futures::sync::oneshot::channel();
	let f = f.then(move |res| tx.send(res).map_err(|_err| ()));
	runtime.spawn(f);

	let watcher = rx.then(|res| res.unwrap())
	                .then(move |res| {
		                complete(res);
		                Ok(())
		               })
	                .join(runtime.shutdown_on_idle());
	std::thread::spawn(move || watcher.wait());

	// future_then_future(f, move |res| {
	// 	match res {
	// 		Result::Ok(v) => ok(v),
	// 		Result::Err(v) => err(v),
	// 	}
	// });
}


pub trait Callable<FO, FE, Ok, Err>
	where Self: Sized,
	      Ok: Send,
	      Err: Send,
	      FO: Send + FnOnce(Ok) -> (),
	      FE: Send + FnOnce(Err) -> () {
}

pub trait Callback<FO, FE, Ok, Err>
	where Self: Sized,
	      Ok: Send,
	      Err: Send,
	      FO: Send + FnOnce(Ok) -> (),
	      FE: Send + FnOnce(Err) -> () {
	fn ok(self) -> Option<FO>;
	fn err(self) -> Option<FE>;

	fn call_ok(self, result: Ok) {
		if let Some(f) = self.ok() {
			f(result)
		};
	}
	fn call_err(self, result: Err) {
		if let Some(f) = self.err() {
			f(result)
		}
	}
	fn call_result(self, result: Result<Ok, Err>) {
		match result {
			Ok(ok) => self.call_ok(ok),
			Err(err) => self.call_err(err),
		}
	}
}

impl<FO, FE, Ok, Err> Callback<FO, FE, Ok, Err> for (Option<FO>, Option<FE>)
	where Ok: Send,
	      Err: Send,
	      FO: Send + FnOnce(Ok) -> (),
	      FE: Send + FnOnce(Err) -> ()
{
	fn ok(mut self) -> Option<FO> { ::std::mem::replace(&mut self.0, None) }
	fn err(mut self) -> Option<FE> { ::std::mem::replace(&mut self.1, None) }
}


pub fn future_then_future<F1: 'static, Ok: 'static, Err: 'static>(f1: F1, f2: ResFn<Ok, Err>)
	where Ok: Send,
	      Err: Send,
	      F1: Send + Future<Item = Ok, Error = Err> {
	let mut runtime = tokio::runtime::Runtime::new().unwrap();
	let (tx, rx) = futures::sync::oneshot::channel();
	let f = f1.then(move |res| tx.send(res).map_err(|_| ()));
	runtime.spawn(f);

	let watcher = rx.then(|res| res.unwrap())
	                .then(move |res| Ok(f2(res)))
	                .join(runtime.shutdown_on_idle());
	std::thread::spawn(move || watcher.wait());
}
