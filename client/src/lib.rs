#![feature(option_replace)]

extern crate url;
extern crate http;
extern crate hyper;
extern crate hyper_tls;
extern crate serde;
extern crate serde_json;
extern crate tokio;
extern crate futures;
use futures::*;

#[macro_use]
extern crate lazy_static;
extern crate mut_static;

pub extern crate gog_api;
use gog_api as gog;
use gog::traits::IClientInfo;
use gog::traits::IClientAuth;
use gog::traits::IClientAuthMut;

pub mod error;
use error::AppError;

pub mod fut;
pub mod net;
pub mod state;
use state::Ctx;
use state::Context;
use state::AuthClient;
use state::Delegate;


pub fn init_from_ctx<F, D>(ctx: &Ctx<'static, D>, online: bool, complete: F) -> Result<(), AppError>
	where F: 'static + Send + FnOnce(Result<(), AppError>) -> (),
	      D: Delegate {
	if online {
		println!("MODE: online");
		init_client_online(ctx, complete)?;
	} else {
		// TODO: imple "offline mode" by cache
		println!("MODE: offline");
	}
	Ok(())
}

pub fn init<F, D>(ctx: &Ctx<'static, D>, state: Option<AuthClient>, online: bool, complete: F) -> Result<(), AppError>
	where F: 'static + Send + FnOnce(Result<(), AppError>) -> (),
	      D: Delegate {
	if let Some(state) = state {
		state::init_hot(&ctx, state)?;
	} else {
		state::init_cold(&ctx)?;
	};

	init_from_ctx(ctx, online, complete)
}

fn init_client_online<F, D>(ctx: &Ctx<'static, D>, complete: F) -> Result<(), AppError>
	where F: 'static + Send + FnOnce(Result<(), AppError>) -> (),
	      D: Delegate {
	use fut::future_then_else;
	use gog::app::TokenState::*;
	use future::Either;

	let cid = ctx.id();
	let delegate = ctx.delegate();

	// upd cfg -> lazy upd auth token -> lazy upd social token:
	let upd_token = upd_config_if_need(&ctx)?.and_then(move |_| {
		                                         // auth token:
		                                         let client = state::gog(cid)?;
		                                         let next = match client.auth_token_state() {
			                                         Expired => {
			                                            println!("main token is expired!");
			                                            Either::A(upd_auth_token(&Ctx::new(cid, delegate))?)
			                                           },
		                                            _ => Either::B(future::ok(())),
		                                         };
		                                         Ok(next)
		                                        })
	                                         .flatten()
	                                         .and_then(move |_| {
		                                         // social token:
		                                         let client = state::gog(cid)?;
		                                         let next = match (client.auth_token_state(), client.social_token_state()) {
			                                         // we can upd social token only when auth token is ok
		                                            (Normal, Expired) | (Normal, Missed) => {
			                                            println!("social token is expired!");
			                                            Either::A(upd_social_token(&Ctx::new(cid, delegate))?)
			                                           },
		                                            _ => Either::B(future::ok(())),
		                                         };
		                                         Ok(next)
		                                        })
	                                         .flatten();
	future_then_else(upd_token, complete);
	// TODO: init_online()?
	// future_then_else(init_online(&ctx)?, complete);
	Ok(())
}

fn init_online<D: Delegate>(ctx: &Ctx<'static, D>) -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	use gog::app::TokenState::*;
	use future::Either;

	let cid = ctx.id();
	let delegate = ctx.delegate();
	// upd cfg -> lazy upd auth token -> lazy upd social token:
	let f = upd_config_if_need(&ctx)?.and_then(move |_| {
		                                 // auth token:
		                                 // let ctx = ctx_to;
		                                 let client = state::gog(cid)?;
		                                 let next = match client.auth_token_state() {
			                                 Expired => {
			                                    println!("main token is expired!");
			                                    Either::A(upd_auth_token(&Ctx::new(cid, delegate))?)
			                                   },
		                                    _ => Either::B(future::ok(())),
		                                 };
		                                 Ok(next)
		                                })
	                                 .flatten()
	                                 .and_then(move |_| {
		                                 // social token:
		                                 // let ctx = ctx_to;
		                                 let client = state::gog(cid)?;
		                                 let next = match (client.auth_token_state(), client.social_token_state()) {
			                                 // we can upd social token only when auth token is ok
		                                    (Normal, Expired) | (Normal, Missed) => {
			                                    println!("social token is expired!");
			                                    Either::A(upd_social_token(&Ctx::new(cid, delegate))?)
			                                   },
		                                    _ => Either::B(future::ok(())),
		                                 };
		                                 Ok(next)
		                                })
	                                 .flatten();
	// let f = f.and_then(move |_| upd_config_if_need(&Ctx::new(cid))).flatten();
	Ok(f)
}


pub fn req_token_with_code<T: AsRef<str>, F, D>(ctx: &Ctx<'static, D>, code: T, complete: F) -> Result<(), AppError>
	where F: 'static + Send + FnOnce(Result<(), AppError>) -> (),
	      D: Delegate {
	use fut::future_then_else;
	let cid = ctx.id();
	let del = ctx.delegate();
	let req_token = {
		                // TODO: opt `.as_ref().to_string()`
		                let req = gog::auth::RequestTokenByCode(code.as_ref().to_string());
		                let f = net::req(req, cid)?;
		                f.map_err(|err| AppError::from(err))
		               }.and_then(move |token| {
		                println!("NEW AUTH TOKEN: {:#?}", token);
		                Ok(*state::gog_mut(cid)?.auth_token_mut() = Some(token))
		               });
	let f = req_token.and_then(move |_| init_online(&Ctx::new(cid, del))).flatten();
	// let f = req_token.and_then(move |_| init_online(ctx)).flatten();
	future_then_else(f, complete);
	Ok(())
}

pub fn req_token_with_code_in_url<T: AsRef<str>, F, D>(url: T, ctx: &Ctx<'static, D>, complete: F) -> Result<(), AppError>
	where F: 'static + Send + FnOnce(Result<(), AppError>) -> (),
	      D: Delegate {
	use std::str::FromStr;
	use url::Url;


	// TODO: use gog::auth::parse_auth_code_in_url
	let u = Url::from_str(url.as_ref())?;
	let mut code = None;
	'parts: for (key, value) in u.query_pairs() {
		if key == "code" {
			code = Some(value);
			break 'parts;
		}
	}
	return if let Some(code) = code {
		req_token_with_code(ctx, code, complete)
	} else {
		Err(AppError::Parse(url::ParseError::EmptyHost))
	};
}


pub fn config_is_needed<D>(_ctx: &Ctx<'_, D>) -> Result<impl Future<Item = bool, Error = AppError>, AppError> {
	let f = future::lazy(|| {
		use gog::app::TokenState::{Expired, Missed, Normal};
		state::cfg_state().map(|st| {
			                  match st {
				                  Expired | Missed => true,
			                     Normal => false,
			                  }
			                 })
	});
	Ok(f.map_err(|err| AppError::from(err)))
}

pub fn get_config<D>(ctx: &Ctx<'_, D>) -> Result<impl Future<Item = gog::cfg::ds::Config, Error = AppError>, AppError> {
	let req = gog::cfg::GetConfig();
	let f = net::req(req, ctx.id())?;
	Ok(f.map_err(|err| AppError::from(err)))
}

pub fn upd_config<D: Delegate>(ctx: &Ctx<'static, D>) -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	let del = ctx.delegate();
	let f = get_config(&ctx)?.and_then(move |cfg| {
		                         if let Some(del) = del {
			                         del.config_upd(&cfg)
			                        };
		                         state::cfg_set(cfg).map_err(|err| AppError::from(err))
		                        });
	Ok(f)
}

pub fn upd_config_if_need<D: Delegate>(ctx: &Ctx<'static, D>) -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	use future::Either;
	let upd = upd_config(&ctx)?;
	let f = config_is_needed(&ctx)?.and_then(|needed| if needed { Either::A(upd) } else { Either::B(future::ok(())) });
	Ok(f)
}


pub fn client_need_login<'a, D>(ctx: &Ctx<'a, D>) -> Result<bool, AppError> {
	Ok(state::is_login_needed(&*state::gog(ctx.id())?))
}

pub fn get_login_uri<'a, D>(_ctx: &Ctx<'a, D>) -> Result<String, AppError> {
	let info = state::info()?;
	let cfg = state::cfg()?;
	let cfg = match *cfg {
		Some(ref cfg) => Some(cfg),
		None => None,
	};
	Ok(gog::auth::get_auth_uri_d(&cfg, info.descriptor()))
}

/// Update auth token without any checks.
pub fn upd_auth_token<D: Delegate>(ctx: &Ctx<'static, D>) -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	let cid = ctx.id();
	let del = ctx.delegate();
	let req = gog::auth::RefreshToken();
	Ok(net::req(req, cid)?.map_err(|err| AppError::from(err)).and_then(move |token| {
		let mut client = state::gog_mut(cid)?;
		*client.auth_token_mut() = Some(token);
		if let Some(del) = del {
			del.auth_token_upd(&*client);
		}
		Ok(())
	}))
}

/// Check if auth token is expired or missed and then request update.
pub fn upd_auth_token_if_need<D: Delegate>(ctx: &Ctx<'static, D>)
                                           -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	use gog::app::TokenState::*;
	use future::Either;

	let client = state::gog(ctx.id())?;
	let f = match client.auth_token_state() {
		Expired => {
			println!("main token is expired!");
			Either::A(upd_auth_token(&ctx)?)
		},
		_ => Either::B(future::ok(())),
	};
	Ok(f)
}

/// Update social token without any checks.
pub fn upd_social_token<D: Delegate>(ctx: &Ctx<'static, D>) -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	let cid = ctx.id();
	let del = ctx.delegate();
	let req = gog::social::MenuAuth();
	Ok(net::req(req, cid)?.map_err(|err| AppError::from(err)).and_then(move |resp| {
		let mut client = state::gog_mut(cid)?;
		*client.social_token_mut() = Some(resp.token);
		if let Some(del) = del {
			// del.tokens_upd(&*client);
			del.social_token_upd(&*client);
			del.social_user_info(resp.user);
		}
		Ok(())
	}))
}

/// Check if social token is expired or missed and then request update.
pub fn upd_social_token_if_need<D: Delegate>(ctx: &Ctx<'static, D>)
                                             -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	use gog::app::TokenState::{Expired, Missed};
	use future::Either;

	let cid = ctx.id();
	let del = ctx.delegate();
	let f = future::lazy(move || {
		        let client = state::gog(cid)?;
		        Ok(match client.social_token_state() {
			        Expired | Missed => {
			           println!("social token is expired!");
			           Either::A(upd_social_token(&Ctx::new(cid, del))?)
			          },
		           _ => Either::B(future::ok(())),
		        })
		       }).map_err(|err: AppError| err)
	        .flatten();
	Ok(f)
}

/// upd_auth_token_if_need => upd_social_token_if_need
pub fn upd_both_tokens_if_need<D: Delegate>(ctx: &Ctx<'static, D>)
                                            -> Result<impl Future<Item = (), Error = AppError>, AppError> {
	let upd_auth = upd_auth_token_if_need(&ctx)?;
	let upd_social = upd_social_token_if_need(ctx)?;
	let f = upd_auth.and_then(|_| upd_social);
	Ok(f)
}
