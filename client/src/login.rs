use std::{self, io};
use std::fmt::Debug;
use futures::{Future, Async, Poll};


pub fn ask_api_key<'a, E, S: AsRef<str>>(uri: S) -> AskCli<String>
	where E: From<<AskCli<String> as Future>::Error> {
	let question = format!(
	                       "Please open your browser go to the url:\n {} \n {}",
	                       uri.as_ref(),
	                       ", login and return the key from URL to me here: \n\t key > "
	);
	AskCli::new(question, true)
}


#[derive(Debug)]
pub struct AskCli<S: Send + AsRef<[u8]>> {
	receiver: Option<std::sync::mpsc::Receiver<String>>,
	question: S,
}

impl<S: Send + AsRef<[u8]>> AskCli<S> {
	pub fn new(question: S, start: bool) -> Self {
		let receiver = None;
		let mut asker = Self { question, receiver };
		if start {
			let _result = asker.init();
		}
		return asker;
	}

	fn init(&mut self) -> Result<(), io::Error> {
		use std::thread;
		use std::sync::mpsc::channel;
		use std::io::{Write, BufRead};

		if self.receiver.is_none() {
			{
				let mut stdout = io::stdout();
				stdout.write_all(&self.question.as_ref())?;
				stdout.flush()?;
			}

			let (sender, receiver) = channel();
			self.receiver = Some(receiver);
			thread::spawn(move || {
				let mut buf = String::new();
				{
					let stdin = io::stdin();
					let mut handle = stdin.lock();
					let _result = handle.read_line(&mut buf);
					// ? shold something with result
				}
				sender.send(buf.trim().to_owned()).unwrap();
			});
		}
		Ok(())
	}
}


impl<S: Send + AsRef<[u8]> + Debug> Future for AskCli<S> {
	type Item = String;
	type Error = std::io::Error;

	fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
		use std::sync::mpsc::TryRecvError;

		self.init()?;
		let res = if let Some(receiver) = &mut self.receiver {
			match receiver.try_recv() {
				Ok(key) => Ok(Async::Ready(key)),
				Err(TryRecvError::Empty) => Ok(Async::NotReady),
				Err(TryRecvError::Disconnected) => Err(io::Error::from(io::ErrorKind::BrokenPipe)),
			}
		} else {
			Err(io::Error::from(io::ErrorKind::Interrupted))
		};
		res
	}

	fn wait(mut self) -> Result<Self::Item, Self::Error>
		where Self: Sized {
		match self.poll()? {
			Async::Ready(result) => Ok(result),
			Async::NotReady => {
				if let Some(receiver) = &mut self.receiver {
					receiver.recv().map_err(|err| {
						io::Error::from(io::ErrorKind::BrokenPipe)
					})
				} else {
					unreachable!()
				}
			},
		}
	}
}
