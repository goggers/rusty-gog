# cargo build -p cffi --target aarch64-apple-ios
# cargo build -p cffi --target armv7-apple-ios
# cargo build -p cffi --target armv7s-apple-ios
# cargo build -p cffi --target i386-apple-ios
# cargo build -p cffi --target x86_64-apple-ios

cd cffi
echo "[CFFI] {DEBUG} build universal (fat) lib"
cargo lipo
cd ../cffi/itests
echo "[CFFI-Tests] {DEBUG} build universal (fat) lib"
cargo lipo
cd ../..

# $(pwd)/$line - to get absolute path by relative to make a correct symlink
echo "make symlinks -> /target/universal/debug/"
ln -sf $(pwd)/$line/target/universal/debug/libgogcffi.a    $(pwd)/$line/target/universal/debug/gog.a
ln -sf $(pwd)/$line/target/universal/debug/libgogcffi.a    $(pwd)/$line/target/universal/gog.a
ln -sf $(pwd)/$line/target/universal/debug/libcffi_tests.a $(pwd)/$line/target/universal/debug/gog_cffi_tests.a
ln -sf $(pwd)/$line/target/universal/debug/libcffi_tests.a $(pwd)/$line/target/universal/gog_cffi_tests.a


# temp: remove / fix me
echo "make symlinks -> /out/bin/debug/"
ln -sf $(pwd)/$line/target/universal/debug/libgogcffi.a    $(pwd)/$line/out/bin/universal/debug/gog.a
ln -sf $(pwd)/$line/target/universal/debug/libcffi_tests.a $(pwd)/$line/out/bin/universal/debug/gog_cffi_tests.a
