# cargo build -p cffi --target aarch64-apple-ios --release
# cargo build -p cffi --target armv7-apple-ios --release
# cargo build -p cffi --target armv7s-apple-ios --release
# cargo build -p cffi --target i386-apple-ios --release
# cargo build -p cffi --target x86_64-apple-ios --release

cd cffi
echo "[CFFI] {RELEASE} build universal (fat) lib"
cargo lipo --release
cd ../cffi/itests
echo "[CFFI-Tests] {RELEASE} build universal (fat) lib"
cargo lipo --release
cd ../..

# $(pwd)/$line - to get absolute path by relative to make a correct symlink
echo "make symlinks -> /target/universal/release/"
ln -sf $(pwd)/$line/target/universal/release/libgogcffi.a    $(pwd)/$line/target/universal/release/gog.a
ln -sf $(pwd)/$line/target/universal/release/libgogcffi.a    $(pwd)/$line/target/universal/gog.a
ln -sf $(pwd)/$line/target/universal/release/libcffi_tests.a $(pwd)/$line/target/universal/release/gog_cffi_tests.a
ln -sf $(pwd)/$line/target/universal/release/libcffi_tests.a $(pwd)/$line/target/universal/gog_cffi_tests.a


# temp: remove / fix me
echo "make symlinks -> /out/bin/release/"
ln -sf $(pwd)/$line/target/universal/release/libgogcffi.a    $(pwd)/$line/out/bin/universal/release/gog.a
ln -sf $(pwd)/$line/target/universal/release/libcffi_tests.a $(pwd)/$line/out/bin/universal/release/gog_cffi_tests.a
