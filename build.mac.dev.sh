TARGET=x86_64-apple-darwin

echo "[CFFI] {DEBUG} build lib ($TARGET)"
cargo build -p cffi --target $TARGET
echo "[CFFI-Tests] {DEBUG} build lib ($TARGET)"
cargo build -p cffi-tests --target $TARGET


echo "make symlinks -> /out/bin/debug/"
ln -sf $(pwd)/$line/target/$TARGET/debug/libgogcffi.a    $(pwd)/$line/out/bin/mac/debug/gog.a
ln -sf $(pwd)/$line/target/$TARGET/debug/libcffi_tests.a    $(pwd)/$line/out/bin/mac/debug/gog_cffi_tests.a
