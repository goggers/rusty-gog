#ifndef rust_abi_h
#define rust_abi_h

/* Warning, this file is handy crafted. Don't modify this manually. */

typedef struct RustString {
//   uintptr_t p;
  const uint8_t *bytes;
  uintptr_t size;
  uintptr_t alignment;
} RustString;


// Fat pointer to the slice of boxed string-buffer. `&str`
typedef struct RustStr {
  const uint8_t *bytes;
  uintptr_t size;
} RustStr;
// Boxed `&str` or `&&str`.
typedef const RustStr* RustStrPtr;


// typedef const [void]* Vec;
// typedef struct Vec<T> {
//   // TODO: Type of el:
//   const T *elements;
//   uintptr_t size;
//   uintptr_t alignment;
// } Vec;

typedef struct Vec_u8 {
  const uint8_t *bytes;
  uintptr_t size;
  uintptr_t alignment;
} Vec;

typedef struct Vec_u64 {
  // const uint8_t *bytes;
  const uint64_t *bytes;
  uintptr_t size;
  uintptr_t alignment;
} Vec_u64;


#endif /* rust_abi_h */
