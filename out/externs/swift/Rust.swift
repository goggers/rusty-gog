

public extension RustString {
	func asUnsafeBufferPointer() -> UnsafeBufferPointer<UInt8> {
		return UnsafeBufferPointer(start: bytes, count: Int(size));
	}

	// /*swift1:*/ func asString(encoding: NSStringEncoding = NSUTF8StringEncoding) -> String? {
	func asString(encoding: String.Encoding = String.Encoding.utf8) -> String? {
		return String(bytes: asUnsafeBufferPointer(), encoding: encoding);
	}
}

public extension RustStr {
	func asUnsafeBufferPointer() -> UnsafeBufferPointer<UInt8> {
		return UnsafeBufferPointer(start: bytes, count: Int(size));
	}

	// /*swift1:*/ func asString(encoding: NSStringEncoding = NSUTF8StringEncoding) -> String? {
	func asString(encoding: String.Encoding = String.Encoding.utf8) -> String? {
		return String(bytes: asUnsafeBufferPointer(), encoding: encoding);
	}
}


//public typealias Vec<T>

public extension Vec_u8 {
	public var length: UInt { get { return self.alignment; } }
	public var capacity: UInt { get { return self.size; } }

	func asUnsafeBufferPointer() -> UnsafeBufferPointer<UInt8> {
		return UnsafeBufferPointer(start: bytes, count: Int(length));
	}
}

public extension Vec_u64 {
	public var length: UInt { get { return self.alignment; } }
	public var capacity: UInt { get { return self.size; } }

	func asUnsafeBufferPointer() -> UnsafeBufferPointer<UInt64> {
		return UnsafeBufferPointer(start: bytes, count: Int(length));
	}
}
