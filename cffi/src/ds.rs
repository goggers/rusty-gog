use cffi::CStrPtr;
use gog::cfg::ds::Config;
use gog::cfg::ds::ConfigEndPoints;
use gog::cfg::ds::ConfigIntervals;
use gog::auth::ds::AuthToken;
use gog::social::ds::SocialToken;
use gog::traits::IClientAuth;


#[repr(C)]
#[no_mangle]
#[derive(Debug)]
pub struct ExportGogClient {
	/// main auth token
	auth_token: *const ExpAuthToken,
	/// social token used for notifications, etc..
	social_token: *const ExpSocialToken,
}

impl ExportGogClient {
	pub fn auth_token(&self) -> Option<&ExpAuthToken> {
		if !self.auth_token.is_null() {
			Some(unsafe { &*self.auth_token })
		} else {
			None
		}
	}
	pub fn social_token(&self) -> Option<&ExpSocialToken> {
		if !self.social_token.is_null() {
			Some(unsafe { &*self.social_token })
		} else {
			None
		}
	}
}

impl<'a, T: IClientAuth> From<&'a T> for ExportGogClient {
	fn from(c: &'a T) -> Self { export_client(c) }
}


#[repr(C)]
#[no_mangle]
#[derive(Debug)]
/// (maybe) Depricated! Use `delegate.on_tokens_upd(...)` instead.
pub struct ExpAuthToken {
	access_token: CStrPtr,
	expires_in: u16,
	token_type: CStrPtr,
	scope: CStrPtr,
	session_id: CStrPtr,
	refresh_token: CStrPtr,
	user_id: CStrPtr,
	timestamp: i64,
}

#[repr(C)]
#[no_mangle]
#[derive(Debug)]
/// (maybe) Depricated! Use `delegate.on_tokens_upd(...)` instead.
pub struct ExpSocialToken {
	pub user_id: CStrPtr,
	pub client_id: CStrPtr,
	pub access_token: CStrPtr,
	pub access_token_expires: i64,
	pub timestamp: i64,
}


impl<'a> From<&'a AuthToken> for ExpAuthToken {
	fn from(t: &AuthToken) -> Self {
		Self { access_token: to_cstring!(t.access_token.token()),
		       expires_in: t.expires_in,
		       token_type: to_cstring!(t.token_type.as_str()),
		       scope: to_cstring!(t.scope.as_str()),
		       session_id: to_cstring!(t.session_id.as_str()),
		       refresh_token: to_cstring!(t.refresh_token.as_str()),
		       user_id: to_cstring!(t.user_id.0.as_str()),
		       timestamp: t.timestamp, }
	}
}

impl<'a> From<&'a SocialToken> for ExpSocialToken {
	fn from(t: &SocialToken) -> Self {
		Self { access_token: to_cstring!(t.access_token.as_str()),
		       access_token_expires: t.access_token_expires,
		       client_id: to_cstring!(t.client_id.as_str()),
		       user_id: to_cstring!(t.user_id.0.as_str()),
		       timestamp: t.timestamp, }
	}
}


impl<'a> Into<AuthToken> for &'a ExpAuthToken {
	fn into(self) -> AuthToken {
		use gog::user::GalaxyUserID;
		use gog::net::headers::Bearer;
		AuthToken { access_token: Bearer::new(from_cstr!(self.access_token)),
		            expires_in: self.expires_in,
		            token_type: from_cstr!(self.token_type).to_owned(),
		            scope: from_cstr!(self.scope).to_owned(),
		            session_id: from_cstr!(self.session_id).to_owned(),
		            refresh_token: from_cstr!(self.refresh_token).to_owned(),
		            user_id: GalaxyUserID(from_cstr!(self.user_id).to_owned()),
		            timestamp: self.timestamp, }
	}
}

impl<'a> Into<SocialToken> for &'a ExpSocialToken {
	fn into(self) -> SocialToken {
		use gog::user::GalaxyUserID;
		SocialToken { user_id: GalaxyUserID(from_cstr!(self.user_id).to_owned()),
		              client_id: from_cstr!(self.client_id).to_owned(),
		              access_token: from_cstr!(self.access_token).to_owned(),
		              access_token_expires: self.access_token_expires,
		              timestamp: self.timestamp, }
	}
}


impl Into<AuthToken> for ExpAuthToken {
	fn into(self) -> AuthToken { (&self).into() }
}

impl Into<SocialToken> for ExpSocialToken {
	fn into(self) -> SocialToken { (&self).into() }
}


pub fn export_client<C: IClientAuth>(client: &C) -> ExportGogClient {
	ExportGogClient { auth_token: if let Some(t) = client.auth_token() {
		                  outbox!(ExpAuthToken::from(t))
		                 } else {
		                  ::std::ptr::null() as *const ExpAuthToken
		                 },
	                  social_token: if let Some(t) = client.social_token() {
		                  outbox!(ExpSocialToken::from(t))
		                 } else {
		                  ::std::ptr::null() as *const ExpSocialToken
		                 }, }
}


// config //

#[repr(C)]
#[no_mangle]
#[derive(Debug)]
/// (maybe) Depricated! Use `delegate.on_config_upd(Config)` instead.
pub struct ExportGogConfig {
	pub status: CStrPtr,
	pub channel: CStrPtr,
	pub talk_interval: u32,
	pub complain_interval: u32,
	pub end_points: ExportConfigEndPoints,
	pub intervals: ExportConfigIntervals,
	pub timestamp: i32,
	pub timestamp_internal: i64,
}

#[repr(C)]
#[no_mangle]
#[derive(Debug)]
/// (maybe) Depricated! Use `delegate.on_config_upd(Config)` instead.
pub struct ExportConfigEndPoints {
	files: CStrPtr,
	products: CStrPtr,
	users: CStrPtr,
	auth: CStrPtr,
	cdn: CStrPtr,
	products_details: CStrPtr,
	gameplay: CStrPtr,
	gog_api: CStrPtr,
}

impl<'a> Into<ConfigEndPoints> for &'a ExportConfigEndPoints {
	fn into(self) -> ConfigEndPoints {
		ConfigEndPoints { files: from_cstr!(self.files).to_owned(),
		                  products: from_cstr!(self.products).to_owned(),
		                  users: from_cstr!(self.users).to_owned(),
		                  auth: from_cstr!(self.auth).to_owned(),
		                  cdn: from_cstr!(self.cdn).to_owned(),
		                  products_details: from_cstr!(self.products_details).to_owned(),
		                  gameplay: from_cstr!(self.gameplay).to_owned(),
		                  gog_api: from_cstr!(self.gog_api).to_owned(), }
	}
}

impl Into<ConfigEndPoints> for ExportConfigEndPoints {
	fn into(self) -> ConfigEndPoints { (&self).into() }
}

impl<'a> From<&'a ConfigEndPoints> for ExportConfigEndPoints {
	fn from(t: &ConfigEndPoints) -> Self {
		Self { files: to_cstring!(t.files.as_str()),
		       products: to_cstring!(t.products.as_str()),
		       users: to_cstring!(t.users.as_str()),
		       auth: to_cstring!(t.auth.as_str()),
		       cdn: to_cstring!(t.cdn.as_str()),
		       products_details: to_cstring!(t.products_details.as_str()),
		       gameplay: to_cstring!(t.gameplay.as_str()),
		       gog_api: to_cstring!(t.gog_api.as_str()), }
	}
}


#[repr(C)]
#[no_mangle]
#[derive(Debug)]
/// (maybe) Depricated! Use `delegate.on_config_upd(Config)` instead.
pub struct ExportConfigIntervals {
	quick: i32,
	presence: i32,
	_short: i32,
	normal: i32,
	self_update_check: i32,
	_long: i32,
	eternity: i32,
}

impl<'a> Into<ConfigIntervals> for &'a ExportConfigIntervals {
	fn into(self) -> ConfigIntervals {
		ConfigIntervals { quick: self.quick,
		                  presence: self.presence,
		                  short: self._short,
		                  normal: self.normal,
		                  self_update_check: self.self_update_check,
		                  long: self._long,
		                  eternity: self.eternity, }
	}
}

impl Into<ConfigIntervals> for ExportConfigIntervals {
	fn into(self) -> ConfigIntervals { (&self).into() }
}

impl<'a> From<&'a ConfigIntervals> for ExportConfigIntervals {
	fn from(t: &ConfigIntervals) -> Self {
		Self { quick: t.quick,
		       presence: t.presence,
		       _short: t.short,
		       normal: t.normal,
		       self_update_check: t.self_update_check,
		       _long: t.long,
		       eternity: t.eternity, }
	}
}


impl<'a> Into<Config> for &'a ExportGogConfig {
	fn into(self) -> Config {
		Config { status: from_cstr!(self.status).to_owned(),
		         channel: from_cstr!(self.channel).to_owned(),
		         talk_interval: self.talk_interval,
		         complain_interval: self.complain_interval,
		         end_points: (&self.end_points).into(),
		         intervals: (&self.intervals).into(),
		         timestamp: self.timestamp,
		         timestamp_internal: self.timestamp_internal, }
	}
}

impl Into<Config> for ExportGogConfig {
	fn into(self) -> Config { (&self).into() }
}

impl<'a> From<&'a Config> for ExportGogConfig {
	fn from(t: &Config) -> Self {
		Self { status: to_cstring!(t.status.as_str()),
		       channel: to_cstring!(t.channel.as_str()),
		       talk_interval: t.talk_interval,
		       complain_interval: t.complain_interval,
		       end_points: (&t.end_points).into(),
		       intervals: (&t.intervals).into(),
		       timestamp: t.timestamp,
		       timestamp_internal: t.timestamp_internal, }
	}
}
