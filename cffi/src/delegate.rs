use gog_client::state::Delegate;
use gog::social::ds::MenuUserInfo;
use gog::social::ds::SocialToken;
use gog::auth::ds::AuthToken;
use gog::traits::IClientAuth;
use gog::cfg::ds::Config;


pub type SocialUserInfo = MenuUserInfo;


pub type AuthTokenPtr = Option<&'static AuthToken>;
pub type SocialTokenPtr = Option<&'static SocialToken>;

#[repr(C)]
#[no_mangle]
#[derive(Debug, Default, Clone)]
/// TODO: docs
pub struct GogDelegate {
	/// When client completely initialized.
	on_init_complete: Option<fn() -> ()>,
	/// Invokes when client gets auth successfully.
	on_login_complete: Option<fn() -> ()>,

	/// Invokes when config updated.
	/// Config is a deep copy of the real used one.
	/// Dealloc it anytime as you wish.
	on_config_upd: Option<fn(Config) -> ()>,

	/// Invokes when any token updated.
	/// Tokens is a deep copies of the real used internals.
	/// Dealloc it anytime as you wish.
	on_tokens_upd: Option<fn(AuthTokenPtr, SocialTokenPtr) -> ()>,

	/// Invokes when client gets new data.
	authed_user_info: Option<fn(SocialUserInfo) -> ()>,
}


impl Delegate for GogDelegate {
	fn initialized(&self) {
		if let Some(f) = &self.on_init_complete {
			f()
		}
	}

	fn logged_in(&self) {
		if let Some(f) = &self.on_login_complete {
			f()
		}
	}

	fn config_upd(&self, v: &Config) {
		if let Some(f) = &self.on_config_upd {
			f(v.clone())
		}
	}

	fn auth_token_upd<T: IClientAuth>(&self, v: &T) {
		if let Some(f) = &self.on_tokens_upd {
			let v = match v.auth_token() {
				Some(t) => {
					let t: AuthToken = t.clone();
					let p = outbox!(t);
					unsafe { p.as_ref() }
				},
				None => None,
			};
			f(v, None)
		}
	}

	fn social_token_upd<T: IClientAuth>(&self, v: &T) {
		if let Some(f) = &self.on_tokens_upd {
			let v = match v.social_token() {
				Some(t) => {
					let t: SocialToken = t.clone();
					let p = outbox!(t);
					unsafe { p.as_ref() }
				},
				None => None,
			};
			f(None, v)
		}
	}

	fn social_user_info(&self, v: SocialUserInfo) {
		if let Some(f) = &self.authed_user_info {
			f(v)
		}
	}
}


/// Creates an empty delegate with all functions is nil (null-ptr).
#[no_mangle]
pub extern "C" fn gog_create_empty_delegate() -> GogDelegate { Default::default() }

/// Creates a delegate with all functions pointing to the `println`.
/// Use it for debug purposes only.
#[no_mangle]
pub extern "C" fn gog_create_printing_delegate() -> GogDelegate {
	GogDelegate { on_init_complete: Some(|| println!("Del.on_init_complete.")),
	              on_login_complete: Some(|| println!("Del.on_login_complete.")),
	              on_config_upd: Some(|v| println!("Del.on_config_upd: {:?}.", v)),
	              on_tokens_upd: Some(|a, s| println!("Del.on_tokens_upd: auth: {:?}, social: {:?}.", a, s)),
	              authed_user_info: Some(|v| println!("Del.authed_user_info: {:?}.", v)), }
}


#[macro_export]
macro_rules! delegate {
	($del:expr, $fn:tt) => {
		if let Some(del) = $del {
			del.$fn()
			}
	};
	($del:expr, $fn:tt, $v:expr) => {
		if let Some(del) = $del {
			del.$fn($v)
			}
	};
}


pub fn send_gog_init<D: Delegate>(d: Option<&D>) { delegate!(d, initialized) }
pub fn send_gog_login_complete<D: Delegate>(d: Option<&D>) { delegate!(d, logged_in) }
