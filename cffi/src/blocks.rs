use result::GogClientError;


pub type ResultBlock<T> = fn(&T) -> ();

/// Block (or C-function ptr) without arguments
/// and returning nothing.
pub type OkBlock = fn() -> ();

/// Block (or C-function ptr) with one argument
/// - C-string representation of caused error
/// and returning nothing.
///
/// __Type of argument wil changed in near future.__
pub type ErrBlock = fn(&GogClientError) -> ();


macro_rules! call_block {
	($result:expr, $ok:expr, $err:expr) => {
		match $result {
			Result::Ok(v) => $ok(),
			Result::Err(v) => $err(&v.into()),
		}
	};
}

macro_rules! call_block_closure {
	($ok:expr, $err:expr) => {
		move |result| call_block!(result, $ok, $err)
	};
}
