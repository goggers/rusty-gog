use blocks::*;
use cffi::*;
use ctx::*;
use ds::*;
use result::NULL_PTR_ERROR;
use gog_client;
use gog_client::state;
use gog_client::state::Context;
use gog::traits::*;

#[cfg(feature = "delegate")]
use delegate;
use delegate::GogDelegate;


/// Export a copy of the existing `Config`.
/// Puts it into the heap and returns pointer.
/// Independent of the context (`Ctx`).
/// (maybe) Depricated! Use `delegate.on_config_upd(Config)` instead.
#[no_mangle]
pub extern "C" fn gog_export_config(err: ErrBlock) -> *const ExportGogConfig {
	let cfg = state::cfg();
	let null = ::std::ptr::null() as *const ExportGogConfig;
	match cfg {
		Ok(cfg) => {
			if let Some(cfg) = &*cfg {
				outbox!(ExportGogConfig::from(cfg))
			} else {
				null
			}
		},
		Err(error) => {
			err(&error.into());
			null
		},
	}
}

/// Import data from `ExportGogConfig` for all contexts and associated clients.
/// Overrides old data if exists.
#[no_mangle]
pub extern "C" fn gog_import_config(cfg: Option<&ExportGogConfig>, err: ErrBlock) {
	if let Some(cfg) = cfg {
		match state::cfg_set(cfg.into()) {
			Ok(_) => {},
			Err(error) => err(&error.into()),
		}
	}
}


/// Create & return new context for the client.
/// If any error is caused the `null pointer` will returned.
/// Internally this method does not creates the client.
/// It only can lock the slot in mem for the client. It's fast & safe.
/// MAX number of contexts and clients = 4.
/// (at this moment the impl is dirty and a little terrible but thread-safe)
#[no_mangle]
pub extern "C" fn gog_create_context(err: ErrBlock) -> *mut GogCtx {
	use std::ptr::null_mut;
	match state::create_context::<'_, GogDelegate>() {
		Ok(ctx) => outbox!(GogCtx::from(&ctx)),
		Err(error) => {
			println!("create_context ERROR: {:?}", error);
			err(&error.into());
			null_mut()
		},
	}
}

/// Creates a context with delegate with all functions pointing to the `println`.
/// Use it for debug purposes only.
#[no_mangle]
pub extern "C" fn gog_create_context_with_printing_delegate(err: ErrBlock) -> *mut GogCtx {
	use std::ptr::null_mut;
	match state::create_context::<'_, GogDelegate>() {
		Ok(mut ctx) => {
			*ctx.delegate_mut() = {
				let del = delegate::gog_create_printing_delegate();
				let ptr = outbox!(del);
				let del = unsafe { ptr.as_ref() };
				del
			};
			outbox!(GogCtx::from(&ctx))
		},
		Err(error) => {
			println!("create_context ERROR: {:?}", error);
			err(&error.into());
			null_mut()
		},
	}
}


/// Export a copy of the existing data associated with context (`Ctx`).
/// Puts it into the heap and returns pointer.
#[no_mangle]
pub extern "C" fn gog_export_context(ctx: Option<&'static GogCtx>, err: ErrBlock) -> *const ExportGogClient {
	use std::ops::Deref;
	let ctx = unwrap(ctx);
	let auth = state::gog(ctx.id());
	match auth {
		Ok(auth) => outbox!(export_client(auth.deref())),
		Err(error) => {
			err(&error.into());
			::std::ptr::null() as *const ExportGogClient
		},
	}
}


/// Import data from `ExportGogClient` into the mem associated with given `Ctx`.
/// Overrides old data if exists.
#[no_mangle]
pub extern "C" fn gog_import_context(ctx: Option<&'static GogCtx>, client: Option<&ExportGogClient>, err: ErrBlock) {
	let ctx = unwrap(ctx);
	let (auth, social) = {
		let tokens = client.expect(NULL_PTR_ERROR);
		(tokens.auth_token().map(|t| t.into()), tokens.social_token().map(|t| t.into()))
	};

	// match state::is_inited::<_, GogDelegate, _>(&ctx.into()) {
	match state::is_inited::<GogDelegate, gog_client::state::Ctx<GogDelegate>>(&ctx.into()) {
		Ok(true) => {
			match state::gog_mut(ctx.id()) {
				Ok(mut c) => {
					*c.auth_token_mut() = auth;
					*c.social_token_mut() = social;
				},
				Err(error) => err(&error.into()),
			}
		},
		Ok(false) => {
			let mut c = state::AuthClient::default();
			*c.auth_token_mut() = auth;
			*c.social_token_mut() = social;
			if let Err(error) = state::init_hot(&ctx.into(), c) {
				err(&error.into());
			}
		},
		Err(error) => {
			err(&error.into());
		},
	}

	println!("imported ctx: {:?} client: {:?}", ctx, &*state::gog(ctx.id()).unwrap());
}


/// if `delegate` was setted and it has `gogClientInitialized` method,
/// the method will be invoked on initialization successful complete.
#[no_mangle]
pub extern "C" fn gog_init_cold(ctx: Option<&'static GogCtx>, ok: OkBlock, err: ErrBlock) {
	let ctx = unwrap(ctx);
	gog_client::init(ctx, None, true, move |res| {
		match res {
			Result::Ok(v) => {
				ok();
				#[cfg(feature = "delegate")]
				delegate::send_gog_init(ctx.delegate());
			},
			Result::Err(v) => err(&v.into()),
		}
	}).unwrap();
}

#[no_mangle]
/// Use instead `gog_init_cold` after `gog_import_context` or to reinit existing client.
pub extern "C" fn gog_init_hot(ctx: Option<&'static GogCtx>, ok: OkBlock, err: ErrBlock) {
	let ctx = unwrap(ctx);
	gog_client::init_from_ctx(&ctx.into(), true, move |res| {
		match res {
			Result::Ok(v) => {
				ok();
				#[cfg(feature = "delegate")]
				delegate::send_gog_init(ctx.delegate());
			},
			Result::Err(v) => err(&v.into()),
		}
	}).unwrap();
}


#[no_mangle]
/// Returns `true` if have no auth token and oauth-login is needed.
pub extern "C" fn gog_need_login(ctx: Option<&'static GogCtx>, err: ErrBlock) -> bool {
	let ctx = unwrap(ctx);
	match gog_client::client_need_login(&ctx.into()) {
		Ok(value) => value,
		Err(error) => {
			err(&error.into());
			false
		},
	}
}


#[no_mangle]
/// Returns URL for oauth-login.
/// We should to direct the user to this url in the web-view
/// and wath any url changes.
pub extern "C" fn gog_get_login_uri(ctx: Option<&'static GogCtx>) -> CStrPtr {
	let login_uri = gog_client::get_login_uri(&ctx.unwrap().into());
	to_cstring!(login_uri.unwrap())
	// state::cached::login_uri(login_uri).into() // for -> ByteSlice
}


#[no_mangle]
/// Requests from server new access-token by given code.
/// code: null terminated string
/// Swift's `String` will casts implicitly.
pub extern "C" fn gog_req_token_with_code(code: CStrPtr, ctx: Option<&'static GogCtx>, ok: OkBlock, err: ErrBlock) {
	let ctx = unwrap(ctx);
	let code = c_string_to_str(code).unwrap();
	#[cfg(feature = "delegate")]
	let ok = move || {
		ok();
		delegate::send_gog_login_complete(ctx.delegate());
	};
	gog_client::req_token_with_code(&ctx.into(), code, call_block_closure!(ok, err)).unwrap();
}

#[no_mangle]
/// Parse URL to get code,
/// Requests from server new access-token by given code.
/// url: null terminated string
/// Swift's `String` will casts implicitly.
pub extern "C" fn gog_req_token_with_url(url: CStrPtr, ctx: Option<&'static GogCtx>, ok: OkBlock, err: ErrBlock) {
	let ctx = unwrap(ctx);
	let url = c_string_to_str(url).unwrap();
	#[cfg(feature = "delegate")]
	let ok = move || {
		ok();
		delegate::send_gog_login_complete(ctx.delegate());
	};
	gog_client::req_token_with_code_in_url(url, &ctx.into(), call_block_closure!(ok, err)).unwrap();
}


#[no_mangle]
/// Check the URL in the login-web-view is final - means success or reject.
/// Returns `true` if if url is final.
/// Returns `false` for any other URL.
/// Can invoke `err` when any error caused (e.g.: url parse error) and then return `false` anyway.
pub extern "C" fn gog_login_url_is_final(url: CStrPtr, err: ErrBlock) -> bool {
	match ::gog::auth::is_auth_uri_final(c_string_to_str(url).unwrap()) {
		Ok(value) => value,
		Err(error) => {
			err(&gog_client::error::AppError::from(error).into());
			false
		},
	}
}
