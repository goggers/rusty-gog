#![allow(unused_variables)]

extern crate libc;
extern crate gog_client;
use gog_client::error::AppError;
pub use gog_client::gog_api as gog;


#[macro_export]
macro_rules! outbox {
	($v:expr) => {
		Box::into_raw(Box::from($v))
	};
}


#[macro_use]
mod cffi;
pub use cffi::*;

pub mod ctx;


#[macro_use]
pub mod blocks;
pub mod result;
pub mod init;
pub mod ds;
// mod temp;

#[cfg(feature = "delegate")]
pub mod delegate;


impl From<gog_client::error::AppError> for ByteSlice where Self: 'static {
	fn from(error: gog_client::error::AppError) -> Self { format!("{:?}", error).into() }
}


// TODO: use #[repr(transparent)]
// https://github.com/rust-lang/rust/pull/52149
