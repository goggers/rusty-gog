use std::fmt;
use std::error::Error as StdError;
use cffi::CStrPtr;
use gog_client::error::AppError;


#[repr(C)]
#[no_mangle]
#[derive(Debug)]
pub struct GogResult<T> {
	// pub result: *const T,
	pub result: T,
}

impl<T> GogResult<T> {
	pub fn new(v: T) -> Self { Self { result: v } }
}


pub type ErrorCode = u8;

#[repr(C)]
#[no_mangle]
#[derive(Debug)]
pub struct GogClientError {
	code: ErrorCode,
	inner: *mut AppError,
}


impl From<AppError> for GogClientError {
	fn from(error: AppError) -> Self {
		let code = err_code(&error);
		let inner = outbox!(error);
		GogClientError { code, inner }
	}
}


impl fmt::Display for GogClientError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "Error #{}: {}", self.code, unsafe { Box::from_raw(self.inner) }.as_ref())
	}
}

impl StdError for GogClientError {}

fn err_code(error: &AppError) -> ErrorCode {
	use AppError::*;
	// TODO: invent a error-codes
	match error {
		Gog(error) => 42,

		Serde(error) => 42,
		Parse(error) => 42,

		Io(error) => 42,
		Tls(error) => 42,
		Net(error) => 42,
		Build(error) => 42,

		MutStatic(error) => 42,
		InvalidCtxId(error) => 42,
		Other(error) => 42,
	}
}

#[no_mangle]
pub extern "C" fn gog_err_to_string(error: Option<&GogClientError>) -> CStrPtr {
	to_cstring!(error.map_or(
		"Error: Can not describe the error because there is null-ptr points into it.".to_string(),
		|err| format!("{}", err),
	))
}

#[no_mangle]
pub extern "C" fn gog_err_description(error: Option<&GogClientError>) -> CStrPtr {
	to_cstring!(error.map_or(
		"Error: Can not describe the error because there is null-ptr points into it.".to_string(),
		|err| format!("{:?}", err.inner),
	))
}

pub const NULL_PTR_ERROR: &str = "Null Pointer Error";
