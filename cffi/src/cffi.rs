#![no_mangle]
#![allow(dead_code)]
#![allow(unused_macros)]
#![allow(unused_imports)]
#![allow(unused_variables)]
//! Swift-CFFI helpers

use std::str;
use std::str::Utf8Error;
use std::ffi;
use std::ffi::{CStr, CString};
use std::ffi::{OsStr, OsString};
use libc::*;


/// Pointer to C-string
pub type CStrPtr = *const c_char;
/// Pointer to mutable C-string
pub type CStrPtrMut = *mut c_char;

/// Reference (fat ptr) to the slice (view) to a boxed string-buffer
pub type RustStr = &'static str;
/// Pointer to the `RustStr`.
pub type RustStrPtr = Box<&'static str>;


pub fn c_string_to_str<'a>(code: CStrPtr) -> Result<&'a str, Utf8Error> {
	use std::str;
	use std::ffi::CStr;
	let c_str: &CStr = unsafe { CStr::from_ptr(code) };
	let byte_slice: &[u8] = c_str.to_bytes();
	str::from_utf8(byte_slice)
}


#[macro_export]
macro_rules! to_cstr {
	($s:expr) => {
		unsafe { ::std::ffi::CStr::from_bytes_with_nul_unchecked($s.as_bytes()).as_ptr() }
	};
}


// Small macro which wraps turning a string-literal into a c-string.
// This is always safe to call, and the resulting pointer has 'static lifetime
#[macro_export]
// add #![feature(macro_literal_matcher)]
// #[cfg(feature = "macro_literal_matcher")]
macro_rules! as_cstr_zeroed {
	// ($s:literal) =>
	($s:expr) => {
		unsafe { ::std::ffi::CStr::from_bytes_with_nul_unchecked(concat!($s, "\0").as_bytes()).as_ptr() }
	};
}


#[macro_export]
macro_rules! to_cstring {
	($s:expr) => {
		::std::ffi::CString::new($s).unwrap().into_raw()
	};
}

#[macro_export]
macro_rules! from_cstr {
	($p:expr) => {
		unsafe { ::std::ffi::CStr::from_ptr($p).to_string_lossy().as_ref() }
	};
}


#[repr(C)]
#[no_mangle]
#[derive(Debug)]
/// Struct with pointer to C-string representation of the borrowed Rust-string
pub struct ByteSlice {
	pub bytes: *const u8,
	pub len: size_t,
}

impl<'a> From<&'a str> for ByteSlice {
	fn from(s: &'a str) -> Self {
		ByteSlice { bytes: s.as_ptr(),
		            len: s.len() as size_t, }
	}
}

impl From<String> for ByteSlice where Self: 'static {
	fn from(s: String) -> Self {
		ByteSlice { bytes: s.as_bytes().as_ptr(),
		            len: s.len() as size_t, }
	}
}


impl Drop for ByteSlice {
	fn drop(&mut self) {
		println!("DROP: {:#?}", self);
	}
}
