use gog_client::state::Context;
use gog_client::state::CID;
use delegate::GogDelegate;


#[repr(C)]
#[derive(Debug, Clone)]
pub struct GogCtx {
	id: u8,
	delegate: Option<&'static GogDelegate>,
}


impl Context<'static, GogDelegate> for GogCtx {
	fn id(&self) -> CID { self.id }
	fn id_ref(&self) -> &CID { &self.id }
	fn delegate(&self) -> Option<&'static GogDelegate> { self.delegate.clone() }
	fn delegate_mut(&mut self) -> &mut Option<&'static GogDelegate> { &mut self.delegate }
}

impl<'a, T: Context<'static, GogDelegate>> From<&'a T> for GogCtx {
	fn from(ctx: &T) -> Self {
		GogCtx { id: ctx.id(),
		         delegate: ctx.delegate(), }
	}
}


#[inline]
pub fn unwrap(ctx: Option<&'static GogCtx>) -> &'static ::gog_client::state::Ctx<'static, GogDelegate> {
	let ctx = ctx.expect(::result::NULL_PTR_ERROR);
	use std::mem::transmute;
	unsafe { transmute::<_, &::gog_client::state::Ctx<'static, GogDelegate>>(ctx) }
}
