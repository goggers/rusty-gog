
echo "copy hand-crafted swift sources"
cp -rf ./src-swift/* ../out/externs/swift/
echo "copy hand-crafted includes"
cp -rf ./include/* ../out/include/


echo "build CFFI externs"
cbindgen -v -c ./cbindgen.toml -o ../out/include/gog.h
