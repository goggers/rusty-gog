import CoreFoundation


public func toString(err: UnsafePointer<GogClientError>?) -> String? {
	return String(cString: gog_err_to_string(err));
}

public func errorCode(err: UnsafePointer<GogClientError>?) -> uint {
	// TODO: write it without `!?`
	return uint((err?.pointee.code)!);
}

public func errorDescription(err: UnsafePointer<GogClientError>?) -> String? {
	return String(cString: gog_err_description(err)); // ?.pointee
}
