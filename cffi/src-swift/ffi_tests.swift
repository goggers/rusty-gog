public func ffi_all_tests() {
	ffi_test_blocks();
	ffi_test_str();
	ffi_test_str_mut();
	ffi_test_strings_ownership();
	ffi_test_strings_ownership_drop();

	ffi_test_exception();
	ffi_test_throw();
}




public func ffi_test_str() {
	let s_ptr = test_get_os_str();
	let str:String = String(cString: s_ptr!);
	assert(TEST_STR == str);
}

public func ffi_test_str_mut() {
	let s_ptr = test_get_os_str_mut();
	let str:String = String(cString: s_ptr!);
	assert(TEST_STR == str);
}


public func ffi_test_strings_ownership() {
	let s_ptr = test_get_os_dyn_str();
	assert(TEST_STR == String(cString: s_ptr!));

	let s_static_ptr = test_get_os_static_string();
	assert(TEST_STR == String(cString: s_static_ptr!));

	let s_dyn_ptr = test_get_os_dyn_string();
	assert(TEST_STR == String(cString: s_dyn_ptr!));
}


// define the storage for ptr and assign value because required.
// So the value doesnt matter, it will be overriden.
var test_str_ptr:CStrPtr = UnsafePointer.init(TEST_STR_WRONG);
func ffi_test_strings_ownership_drop_callback(ptr: CStrPtr?) {
	print("ffi_test_strings_ownership_drop_callback: \(String(cString: test_str_ptr)) <- \(String(cString: ptr!))");
	assert(TEST_STR == String(cString: ptr!));
	test_str_ptr = ptr!;
}

public func ffi_test_strings_ownership_drop() {
	test_get_os_str_to_block(ffi_test_strings_ownership_drop_callback);
	print("S out of block: \(String(cString: test_str_ptr))");
	assert(TEST_STR == String(cString: test_str_ptr));
}


public func ffi_test_blocks() {
	test_block_1({ (slice: ByteSlice) in
//		print("OK: \(String(describing: slice.asString()))");
		assert(TEST_STR == slice.asString()!);
	});

	test_block_2({ print("OK"); }, { (err_ptr) in
		let err = err_ptr?.pointee;
		print("error: \(err?.code, err?.description.asString())");
	});

	test_block_3({ (ok) in
		print("OK: \(String(describing: ok.result.asString()))");
	}, { (code, description) in
		print("error: \(code, description.asString())");
	});
}


public func ffi_test_exception() {
	let ptr = test_ret_exception();
	print("exception: \(ptr)");
	let exception:NSException = test_ret_exception();
	print("exception: \(exception.name.rawValue, exception.reason, exception.userInfo, exception.callStackSymbols)");
}


public func ffi_test_throw() {
	do {
		try test_throw();
		print("test_throw: FAIL");
	} catch (let err) {
		print("test_throw: OK");
		print("catched exception: \(err)");
	}
	print("test_throw: end");
}

// Rust ABI tests:

public func rust_abi_all_tests() {
	rust_abi_string_tests();
	rust_abi_str_tests();
	rust_abi_vec_tests();
}

public func rust_abi_string_tests() {
	let rstr_1 = rust_test_get_string_static_ref();
	assert("" == rstr_1?.pointee.asString());
	rust_test_mod_string_static();
	assert(TEST_STR == rstr_1?.pointee.asString());

	let rstr_2 = rust_test_get_string_boxed_ref();
	assert(TEST_STR == rstr_2?.pointee.asString());

	let rstr_5 = rust_test_get_string();
	assert(TEST_STR == rstr_5.asString());
}

public func rust_abi_str_tests() {
	let rstr_6 = rust_test_get_str_static_ref_ref();
	assert(TEST_STR == rstr_6?.pointee.asString());

	let rstr_7 = rust_test_get_str_boxed_ref();
	assert(TEST_STR == rstr_7?.pointee.asString());
}

public func rust_abi_vec_tests() {
	let vec = rust_test_get_vec_ref();
	assert(0 == vec?.pointee.length);
	rust_test_mod_vec__push(42);
	assert(1 == vec?.pointee.length);
	rust_test_mod_vec__push(42);
	assert(2 == vec?.pointee.length);
	rust_test_mod_vec__push(42);
	assert(3 == vec?.pointee.length);
	assert(3 < (vec?.pointee.capacity)!);
	rust_test_mod_vec__push(42);
	assert(4 == vec?.pointee.length);
	assert(4 <= (vec?.pointee.capacity)!);

	let buf_ptr = vec?.pointee.asUnsafeBufferPointer();
	assert(4 == buf_ptr?.count);

	let iter = buf_ptr?.makeIterator();
	for element in iter! {
		assert(42 == element);
	}
}
