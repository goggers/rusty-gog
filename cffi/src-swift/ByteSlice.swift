import CoreFoundation


public extension ByteSlice {
	func asUnsafeBufferPointer() -> UnsafeBufferPointer<UInt8> {
		return UnsafeBufferPointer(start: bytes, count: len);
	}

	// /*swift1:*/ func asString(encoding: NSStringEncoding = NSUTF8StringEncoding) -> String? {
	func asString(encoding: String.Encoding = String.Encoding.utf8) -> String? {
		return String(bytes: asUnsafeBufferPointer(), encoding: encoding);
	}
}
