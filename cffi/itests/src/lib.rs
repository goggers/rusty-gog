//! Intergation-Tests for Swift-CFFI helpers
#![feature(const_string_new)]
#![feature(const_vec_new)]
#![no_mangle]

use std::str;
use std::mem;
use std::ffi;


#[macro_use]
extern crate objc;
extern crate objc_exception;


#[macro_use]
extern crate gogcffi;
use gogcffi::*;


pub mod objc_extras {
	use super::*;
	use objc::runtime::Object;
	use std::ops::{Deref, DerefMut};

	pub struct StrongPtr(*mut Object);
	impl Deref for StrongPtr {
		type Target = Object;
		fn deref(&self) -> &Object { unsafe { &*self.0 } }
	}
	impl DerefMut for StrongPtr {
		fn deref_mut(&mut self) -> &mut Object { unsafe { &mut *self.0 } }
	}
	impl Drop for StrongPtr {
		fn drop(&mut self) {
			let _: () = unsafe {
				println!("obj ptr dropped");
				msg_send![self.0, release]
			};
		}
	}


	static UTF8_ENCODING: u32 = 4;

	pub fn str_to_nsstr(content: &str) -> *mut Object {
		let ptr: *mut Object = unsafe {
			let cls = class!(NSString);
			for ivar in cls.instance_variables().iter() {
				println!("\t s.{}", ivar.name());
			}

			let string: *mut Object = msg_send![cls, alloc];
			msg_send![string, initWithBytes:content.as_ptr()
			                  length:content.len()
			                  encoding:UTF8_ENCODING]
		};
		ptr
	}


	pub fn nil() -> *mut objc::runtime::Object { ::std::ptr::null_mut() }

	pub fn exception<N, R>(name: N, reason: R, user_data: Option<*mut objc::runtime::Object>) -> StrongPtr
		where N: AsRef<str>,
		      R: AsRef<str> {
		let user_data = user_data.unwrap_or(::std::ptr::null_mut());
		let cls = class!(NSException);
		unsafe {
			StrongPtr({
				let obj: *mut Object = msg_send![cls, alloc];
				let name = str_to_nsstr(name.as_ref());
				let reason = str_to_nsstr(reason.as_ref());
				msg_send![obj, initWithName:name reason:reason userInfo:user_data]
			})
		}
	}
}


// #[no_mangle] pub
static TEST_STR: &'static str = "|https://login.gog.com/foo/bar/blah/blah/blah|";
macro_rules! TEST_STR {
	() => {
		"|https://login.gog.com/foo/bar/blah/blah/blah|"
	};
}


/// doc for SimpleBlock
pub type SimpleBlock = fn() -> ();
/// doc for ErrPtrBlock
pub type ErrPtrBlock = fn(*const TestError) -> ();
/// doc for TestErrBlock
pub type TestErrBlock = fn(u32, ByteSlice) -> ();
/// doc for TestResultBlock<T>
pub type TestResultBlock<T> = fn(T) -> ();


#[repr(C)]
#[no_mangle]
#[derive(Debug)]
pub struct TestResult<T> {
	pub result: T,
}

impl<T> TestResult<T> {
	pub fn new(v: T) -> Self { Self { result: v } }
}


#[repr(C)]
#[no_mangle]
#[derive(Debug)]
pub struct TestError {
	pub code: u32,
	pub description: ByteSlice,
}

impl TestError {
	pub fn new(code: u32) -> Self {
		let description = "some description".into();
		Self { code, description }
	}

	pub fn custom<T: AsRef<str>>(code: u32, description: T) -> Self {
		let description = description.as_ref().into();
		Self { code, description }
	}
}


#[no_mangle]
pub extern "C" fn test_get_os_str() -> CStrPtr { as_cstr_zeroed!(TEST_STR!()) }
#[no_mangle]
pub extern "C" fn test_get_os_str_mut() -> CStrPtrMut { as_cstr_zeroed!(TEST_STR!()) as CStrPtrMut }


#[no_mangle]
pub extern "C" fn test_get_os_dyn_str() -> CStrPtr {
	static mut BUF: String = String::new();
	let mut buf = String::from("|https://login.gog.com/foo/bar/");
	buf.push_str("blah/blah/blah|");
	unsafe { BUF = buf }
	unsafe { ffi::CStr::from_bytes_with_nul_unchecked(BUF.as_bytes()).as_ptr() }
}

#[no_mangle]
pub extern "C" fn test_get_os_static_string() -> CStrPtr { to_cstring!(TEST_STR) }

#[no_mangle]
/// returns borrowed C-string
pub extern "C" fn test_get_os_dyn_string() -> CStrPtr {
	let mut buf = String::from("|https://login.gog.com/foo/bar/");
	buf.push_str("blah/blah/blah|");
	to_cstring!(buf)
}


#[no_mangle]
/// takes a block with one arg - `TestResult with inner c-string`
pub extern "C" fn test_get_os_str_to_block(f: TestResultBlock<CStrPtr>) {
	{
		let buf = String::from(TEST_STR);
		{
			f(to_cstring!(buf.as_str()));
		}
		f(to_cstring!(buf));
	}
	println!("rust:: source string was dropped");
}


#[no_mangle]
pub unsafe extern "C" fn test_throw() {
	if !cfg!(feature = "exception") {
		println!("rust:: Exception throwing disabled because compiled without `exception` feature.");
		return;
	}

	let exception = {
		let mut obj = objc_extras::exception("|name|", "|reason|", None);
		msg_send![obj, retain];
		obj
	};
	msg_send![exception, raise];
	msg_send![exception, release];
}

#[no_mangle]
pub unsafe extern "C" fn test_ret_exception() -> *mut ::objc_exception::Exception {
	let mut obj = objc_extras::exception("|the name|", "|the reason|", None);
	msg_send![obj, retain];
	let ptr: *mut _ = &mut *obj;
	ptr as *mut _ as *mut ::objc_exception::Exception
}


#[no_mangle]
pub extern "C" fn test_nil_result() -> TestResult<ByteSlice> { unsafe { mem::uninitialized() } }
#[no_mangle]
pub extern "C" fn test_nil_error() -> TestError { unsafe { mem::uninitialized() } }


#[no_mangle]
pub extern "C" fn test_block_1(f: TestResultBlock<ByteSlice>) { f(TEST_STR.into()); }

#[no_mangle]
pub extern "C" fn test_block_2(ok: SimpleBlock, err: ErrPtrBlock) {
	ok();
	let e = TestError::new(42);
	let ptr = &e as *const TestError;
	err(ptr);
}

#[no_mangle]
pub extern "C" fn test_block_3(ok: TestResultBlock<TestResult<ByteSlice>>, err: TestErrBlock) {
	ok(TestResult { result: "Ooooookay".into(), });
	err(42, "err descr".into());
}


// Rust ABI

pub mod abi_tests {
	use super::*;

	static mut STRING: String = String::new();
	static mut VEC: Vec<u64> = Vec::new();

	#[no_mangle]
	pub extern "C" fn rust_test_mod_string_static() { unsafe { STRING.push_str(TEST_STR) } }

	#[no_mangle]
	pub extern "C" fn rust_test_get_string_static_ref() -> &'static String { unsafe { &STRING } }

	#[no_mangle]
	pub extern "C" fn rust_test_get_string_boxed_ref() -> &'static String {
		use std::mem::transmute;
		let boxed: Box<String> = Box::from(String::from(TEST_STR));
		let boxed = Box::into_raw(boxed);
		unsafe { transmute::<_, &String>(boxed) }
	}

	#[no_mangle]
	pub extern "C" fn rust_test_get_string() -> String { String::from(TEST_STR) }


	pub type RustStr = &'static str;
	pub type RustStrPtr = Box<&'static str>;

	#[no_mangle]
	pub extern "C" fn rust_test_get_str_static_ref_ref() -> &'static RustStr {
		let ptr = Box::into_raw(Box::from(unsafe { STRING.as_str() }));
		return unsafe { &*ptr };
	}

	#[no_mangle]
	pub extern "C" fn rust_test_get_str_boxed_ref() -> RustStrPtr { Box::from(unsafe { STRING.as_str() }) }

	#[no_mangle]
	pub extern "C" fn rust_test_get_vec_ref() -> &'static Vec<u64> { unsafe { &VEC } }

	#[no_mangle]
	pub extern "C" fn rust_test_mod_vec__push(v: u64) { unsafe { VEC.push(v) } }
}
